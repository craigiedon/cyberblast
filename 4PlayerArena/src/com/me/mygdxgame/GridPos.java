package com.me.mygdxgame;

public class GridPos {
	public int r;
	public int c;
	
	public GridPos(int row, int col){
		r = row;
		c = col;
	}
}

package com.me.mygdxgame;

public enum TileTag {
	FLOOR, INDESTRUCTIBLE_BLOCK, DESTRUCTIBLE_BLOCK, HOLE, P1_START, P2_START, P3_START, P4_START
}

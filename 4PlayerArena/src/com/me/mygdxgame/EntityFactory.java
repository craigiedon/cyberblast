package com.me.mygdxgame;

import com.artemis.Entity;
import com.artemis.World;
import com.artemis.managers.GroupManager;
import com.artemis.utils.ImmutableBag;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.me.mygdxgame.components.Bomb;
import com.me.mygdxgame.components.Collidable;
import com.me.mygdxgame.components.Direction;
import com.me.mygdxgame.components.Expires;
import com.me.mygdxgame.components.Facing;
import com.me.mygdxgame.components.Hole;
import com.me.mygdxgame.components.Homing;
import com.me.mygdxgame.components.Inventory;
import com.me.mygdxgame.components.Oscillates;
import com.me.mygdxgame.components.Owned;
import com.me.mygdxgame.components.PlayerInfo;
import com.me.mygdxgame.components.Position;
import com.me.mygdxgame.components.PowerType;
import com.me.mygdxgame.components.Powerup;
import com.me.mygdxgame.components.Renderable;
import com.me.mygdxgame.components.Moveable;
import com.me.mygdxgame.systems.BombSystem;
import com.me.mygdxgame.systems.LevelSystem;

public class EntityFactory {
	
	private final static int bombSize = 25;
	private final static int playerSize = 35;
	
	public static Entity playerFactory(World world, int playerNum, Controller controller, CharacterType charType, int x, int y, TextureAtlas atlas){
		Entity player = world.createEntity();
		player.addComponent(new PlayerInfo(controller, playerNum, charType));
		player.addComponent(new Renderable(atlas.findRegion("robotWalk" + playerNum), playerSize, playerSize));
		player.addComponent(new Position(x,y));
		player.addComponent(new Moveable());
		player.addComponent(new Collidable(playerSize, playerSize));
		player.addComponent(new Inventory());
		player.addComponent(new Facing(Direction.DOWN));
		GroupManager groupManager = world.getManager(GroupManager.class);
		groupManager.add(player, GroupTags.PLAYER);
		player.addToWorld();
		return player;
	}
	
	public static Entity holeFactory(World world, int row, int col, TextureAtlas atlas){
		int tileSize = LevelSystem.tileSize;
		Entity hole = world.createEntity();
		hole.addComponent(new Position(col * tileSize, row * tileSize));
		hole.addComponent(new Renderable(atlas.findRegion("holeTile"), tileSize, tileSize));
		hole.addComponent(new Collidable(tileSize, tileSize));
		hole.addComponent(new Hole());
		hole.addToWorld();
		world.getManager(GroupManager.class).add(hole,  GroupTags.HOLE);
		
		return hole;
	}
	
	public static Entity bombFactory(World world, float x, float y, TextureAtlas atlas, BombBehaviour bombBehaviour, int playerNum, float fuse, int spreadingPower){
		Entity bomb = world.createEntity();
		bomb.addComponent(new Position(x - bombSize / 2, y - bombSize / 2));
		bomb.addComponent(new Renderable(atlas.findRegion(BombSystem.bombImages.get(bombBehaviour)), bombSize, bombSize));
		bomb.addComponent(new Collidable(bombSize, bombSize));
		bomb.addComponent(new Moveable());
		bomb.addComponent(new Bomb(fuse, bombBehaviour, spreadingPower));
		bomb.addComponent(new Owned(playerNum));
		if(bombBehaviour == BombBehaviour.HOMING){
			bomb.addComponent(new Homing());
		}
		bomb.addToWorld();
		world.getManager(GroupManager.class).add(bomb, GroupTags.BOMB);
		
		return bomb;
	}

	public static Entity floorFactory(World world, int row, int col, TextureAtlas atlas) {
		Entity floor = world.createEntity();
		floor.addComponent(new Position(col * LevelSystem.tileSize, row * LevelSystem.tileSize));
		floor.addComponent(new Renderable(atlas.findRegion("floorTile"), LevelSystem.tileSize, LevelSystem.tileSize));
		floor.addToWorld();
		
		return floor;
	}
	
	public static Entity powerupFactory(World world, int row, int col, TextureAtlas atlas, PowerType pt){
		Entity powerup = world.createEntity();
		String powerImage;
		if(pt == PowerType.BOMB_AMOUNT){
			powerImage = "bombPowerup"; 
		}
		else if(pt == PowerType.BOMB_SPREAD){
			powerImage = "spreadPowerup";
		}
		else{
			powerImage = "speedPowerup";
		}
		powerup.addComponent(new Position());
		powerup.addComponent(new Renderable(atlas.findRegion(powerImage), LevelSystem.tileSize / 2, LevelSystem.tileSize / 2));
		powerup.addComponent(new Collidable(LevelSystem.tileSize / 2, LevelSystem.tileSize / 2));
		powerup.addComponent(new Powerup(pt));
		powerup.addComponent(new Expires(6));
		powerup.addComponent(new Oscillates(0, 3));
		powerup.addToWorld();
		LevelSystem.placeAtTile(powerup, row, col);
		world.getManager(GroupManager.class).add(powerup, GroupTags.POWERUP);
		
		return powerup;
	}
}

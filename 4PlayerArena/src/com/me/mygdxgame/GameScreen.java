package com.me.mygdxgame;
import com.artemis.Entity;
import com.artemis.World;
import com.artemis.managers.GroupManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.me.mygdxgame.components.Collidable;
import com.me.mygdxgame.components.PlayerInfo;
import com.me.mygdxgame.components.Position;
import com.me.mygdxgame.components.Renderable;
import com.me.mygdxgame.components.Moveable;
import com.me.mygdxgame.systems.AnimationSystem;
import com.me.mygdxgame.systems.BombSystem;
import com.me.mygdxgame.systems.ExpirySystem;
import com.me.mygdxgame.systems.ForceChargingSystem;
import com.me.mygdxgame.systems.HomingSystem;
import com.me.mygdxgame.systems.LevelSystem;
import com.me.mygdxgame.systems.MovementSystem;
import com.me.mygdxgame.systems.PlayerCollisionSystem;
import com.me.mygdxgame.systems.PowerupPlacementSystem;
import com.me.mygdxgame.systems.RenderingSystem;
import com.me.mygdxgame.systems.StunSystem;


public class GameScreen implements Screen{
	final MyGdxGame game;
	OrthographicCamera camera;
	TextureAtlas atlas;
	Array<Controller> gameControllers;
	World world;
	MovementSystem movementSystem;
	BombSystem bombSystem;
	LevelSystem levelSystem;
	ForceChargingSystem forceChargingSystem;
	
	public GameScreen(MyGdxGame g){
		game = g;
		
		Array<PlayerInfo> participatingPlayers = new Array<PlayerInfo>();
		Array<Controller> controllers = Controllers.getControllers();
		for(int i = 0; i < controllers.size; i++){
			Controller controller = controllers.get(i);
			participatingPlayers.add(new PlayerInfo(controller, i + 1, CharacterType.FORCE));
		}
		
		Json json = new Json();
		LevelInfo levelInfo = json.fromJson(LevelInfo.class, Gdx.files.local("customLevels/level1.json"));
		init(participatingPlayers, levelInfo);
	}
	
	public GameScreen(MyGdxGame g, Array<PlayerInfo> participatingPlayers, LevelInfo levelInfo) {
		game = g;
		init(participatingPlayers, levelInfo);
	}
	
	public void init(Array<PlayerInfo> participatingPlayers, LevelInfo levelInfo){
		camera = new OrthographicCamera();
		camera.setToOrtho(false);
		atlas = new TextureAtlas(Gdx.files.internal("placeHolderGraphics.atlas"));
		
		world = new World();
		movementSystem = new MovementSystem();
		world.setSystem(movementSystem);
		world.setSystem(new HomingSystem());
		world.setSystem(new PlayerCollisionSystem());
		world.setSystem(new RenderingSystem(game.batch));
		world.setSystem(new AnimationSystem(game.batch));
		bombSystem = new BombSystem(atlas);
		world.setSystem(bombSystem);
		world.setSystem(new ExpirySystem());
		levelSystem = new LevelSystem(levelInfo, participatingPlayers, atlas, game);
		world.setSystem(levelSystem);
		forceChargingSystem = new ForceChargingSystem(atlas);
		world.setSystem(forceChargingSystem);
		world.setSystem(new StunSystem());
		world.setSystem(new PowerupPlacementSystem(atlas));

		
		GroupManager groupManager = new GroupManager();
		world.setManager(groupManager);
		
		world.initialize();
		Controllers.addListener(movementSystem);
		Controllers.addListener(bombSystem);
		Controllers.addListener(levelSystem);
		Controllers.addListener(forceChargingSystem);
		
		Vector2 levelDims = levelSystem.calculateLevelDimensions();
		camera.translate((int)-(camera.viewportWidth - levelDims.x) / 2, (int)-(camera.viewportHeight - levelDims.y) / 2);
	}


	@Override
	public void render(float arg0) {
		Gdx.gl.glClearColor(0,0,0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		camera.update();
		game.batch.setProjectionMatrix(camera.combined);
		
		world.setDelta(Gdx.graphics.getDeltaTime());
		world.process();
		
		if(levelSystem.isGameOver()){
			Controllers.removeListener(movementSystem);
			Controllers.removeListener(bombSystem);
			Controllers.removeListener(levelSystem);
			Controllers.removeListener(forceChargingSystem);
			game.setScreen(new WinScreen(game, levelSystem.originalLevelInfo, levelSystem.participatingPlayers, levelSystem.winningPlayer));
		}
	}
	
	@Override
	public void dispose() {
		// TODO Auto-generated method stub
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void resize(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

}

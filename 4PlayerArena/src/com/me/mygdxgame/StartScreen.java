package com.me.mygdxgame;

import com.artemis.Entity;
import com.artemis.World;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.me.mygdxgame.components.ControllerInfo;
import com.me.mygdxgame.components.Position;
import com.me.mygdxgame.components.TextRenderable;
import com.me.mygdxgame.systems.RenderingSystem;
import com.me.mygdxgame.systems.TextRenderingSystem;

public class StartScreen implements Screen, ControllerListener {
	
	MyGdxGame game;
	OrthographicCamera camera;
	BitmapFont videoGameFont72;
	BitmapFont videoGameFont32;
	World world;
	Array<ControllerInfo> controllersInfo;
	final float DEAD_ZONE = 0.3f;
	int menuSelectIndex;
	StartMenuState[] menuStates;
	TextureAtlas atlas;
	TextRenderable playTextRenderable;
	final int HIGHLIGHT_HEIGHT = 50;
	boolean startGamePressed = false;
	
	public StartScreen(MyGdxGame g){
		game = g;
		camera = new OrthographicCamera();
		camera.setToOrtho(false);
		atlas = new TextureAtlas(Gdx.files.internal("placeHolderGraphics.atlas"));
		videoGameFont72 = game.videoGameFontGenerator.generateFont(72);
		videoGameFont32 = game.videoGameFontGenerator.generateFont(32);
		
		world = new World();
		world.setSystem(new RenderingSystem(game.batch));
		world.setSystem(new TextRenderingSystem(camera));
		
		Entity titleText = world.createEntity();
		titleText.addComponent(new Position(400, 400));
		titleText.addComponent(new TextRenderable("Cyber Blast", videoGameFont72, true));
		titleText.addToWorld();
		
		Entity playText = world.createEntity();
		playText.addComponent(new Position(400, 200));
		playTextRenderable = new TextRenderable("Play", videoGameFont32, true);
		playText.addComponent(playTextRenderable);
		playText.addToWorld();
		
		Entity levelEditorText = world.createEntity();
		levelEditorText.addComponent(new Position(400, 100));
		levelEditorText.addComponent(new TextRenderable("Level Editor", videoGameFont32, true));
		levelEditorText.addToWorld();
		
		menuSelectIndex = 0;
		menuStates = StartMenuState.values();
		
		controllersInfo = new Array<ControllerInfo>();
		for(Controller controller : Controllers.getControllers()){
			controllersInfo.add(new ControllerInfo(controller, ThumbStickPos.DEAD_ZONE));
		}
		Controllers.addListener(this);
		
		
		world.initialize();
	}
	
	@Override
	public void render(float arg0) {
		Gdx.gl.glClearColor(0,0,0,1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		camera.update();
		game.batch.setProjectionMatrix(camera.combined);
		

		for(ControllerInfo contInfo : controllersInfo){
			ThumbStickPos newLeftAnaloguePos = XBoxConfig.convertToUpDown(contInfo.controller.getAxis(XBoxConfig.AXIS_LEFT_X), contInfo.controller.getAxis(XBoxConfig.AXIS_LEFT_Y), DEAD_ZONE);
			
			if(newLeftAnaloguePos != contInfo.prevLeftAnalogueState){
				switch(newLeftAnaloguePos){
				case DEAD_ZONE:
					break;
				case UP:
					menuSelectIndex = (menuSelectIndex + 1) % menuStates.length;
					break;
				case DOWN:
					menuSelectIndex = (menuSelectIndex + 1) % menuStates.length;
					break;
				default:
					System.out.println("Invalid thumbstick direction. How did you get here?");
					break;
				}
			}
			
			contInfo.prevLeftAnalogueState = newLeftAnaloguePos;
		}

		game.batch.begin();
		if(menuStates[menuSelectIndex] == StartMenuState.PLAY){
			game.batch.draw(atlas.findRegion("exampleBlock6"), 0, 200 - HIGHLIGHT_HEIGHT / 2, camera.viewportWidth, HIGHLIGHT_HEIGHT);
		}
		else{
			game.batch.draw(atlas.findRegion("exampleBlock6"), 0, 100 - HIGHLIGHT_HEIGHT / 2, camera.viewportWidth, HIGHLIGHT_HEIGHT);
		}
		game.batch.end();
		
		world.setDelta(Gdx.graphics.getDeltaTime());
		world.process();
		
		if(startGamePressed){
			switch(menuStates[menuSelectIndex]){
			case PLAY:
				game.setScreen(new PlayerSelectScreen(game));
				break;
			case LEVEL_EDIT:
				game.setScreen(new LevelEditorScreen(game));
				break;
			}
		}
	}
	
	
	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resize(int arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean accelerometerMoved(Controller arg0, int arg1, Vector3 arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean axisMoved(Controller arg0, int arg1, float arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean buttonDown(Controller controller, int buttonCode) {
		if(buttonCode == XBoxConfig.BUTTON_A || buttonCode == XBoxConfig.BUTTON_START){
			Controllers.removeListener(this);
			startGamePressed = true;
		}
		return false;
	}

	@Override
	public boolean buttonUp(Controller arg0, int arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void connected(Controller arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disconnected(Controller arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean povMoved(Controller controller, int povCode, PovDirection povValue) {
		if(povValue == PovDirection.north || povValue == PovDirection.south){
			menuSelectIndex = (menuSelectIndex + 1) % menuStates.length;
		}
		return false;
	}

	@Override
	public boolean xSliderMoved(Controller arg0, int arg1, boolean arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean ySliderMoved(Controller arg0, int arg1, boolean arg2) {
		// TODO Auto-generated method stub
		return false;
	}

}

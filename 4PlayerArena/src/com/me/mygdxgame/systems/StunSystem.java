package com.me.mygdxgame.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import com.artemis.systems.EntityProcessingSystem;
import com.me.mygdxgame.components.Moveable;
import com.me.mygdxgame.components.Stunned;

public class StunSystem extends EntityProcessingSystem{
	@Mapper ComponentMapper<Stunned> stunMap;
	@Mapper ComponentMapper<Moveable> movMap;
	
	
	@SuppressWarnings("unchecked")
	public StunSystem() {
		super(Aspect.getAspectForAll(Stunned.class, Moveable.class));
	}

	@Override
	protected void process(Entity e) {
		Stunned stun = stunMap.get(e);
		Moveable moveable = movMap.get(e);
		
		stun.duration -= world.getDelta();
		
		if(stun.duration <= 0){
			e.removeComponent(stun);
			e.addToWorld();
		}
	}

}

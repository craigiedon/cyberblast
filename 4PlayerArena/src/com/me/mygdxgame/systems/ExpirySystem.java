package com.me.mygdxgame.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import com.artemis.systems.EntityProcessingSystem;
import com.me.mygdxgame.components.Expires;

public class ExpirySystem extends EntityProcessingSystem{
	@Mapper ComponentMapper<Expires> expMap;
	
	@SuppressWarnings("unchecked")
	public ExpirySystem() {
		super(Aspect.getAspectForAll(Expires.class));
	}

	@Override
	protected void process(Entity e) {
		Expires expiry = expMap.get(e);
		expiry.timeLeft -= world.getDelta();
		
		if(expiry.timeLeft <= 0){
			e.deleteFromWorld();
		}
	}
	
}

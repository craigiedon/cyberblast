package com.me.mygdxgame.systems;

import java.util.LinkedList;
import java.util.List;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import com.artemis.managers.GroupManager;
import com.artemis.systems.EntityProcessingSystem;
import com.artemis.systems.VoidEntitySystem;
import com.artemis.utils.ImmutableBag;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.utils.Array;
import com.me.mygdxgame.GridPos;
import com.me.mygdxgame.GroupTags;
import com.me.mygdxgame.components.Collidable;
import com.me.mygdxgame.components.Direction;
import com.me.mygdxgame.components.Homing;
import com.me.mygdxgame.components.Moveable;
import com.me.mygdxgame.components.Owned;
import com.me.mygdxgame.components.PlayerInfo;
import com.me.mygdxgame.components.Position;

public class HomingSystem extends EntityProcessingSystem{
	@Mapper ComponentMapper<Owned> ownMap;
	@Mapper ComponentMapper<PlayerInfo> playerMap;
	@Mapper ComponentMapper<Homing> homingMap;
	@Mapper ComponentMapper<Position> posMap;
	@Mapper ComponentMapper<Moveable> moveMap;
	@Mapper ComponentMapper<Collidable> colMap;
	
	final int HOMING_SPEED = 75;
	final int ADJUST_SPEED = 75;
	
	@SuppressWarnings("unchecked")
	public HomingSystem(){
		super(Aspect.getAspectForAll(Homing.class, Moveable.class));
	}
	
	@Override
	protected void begin(){
	}
	
	@Override
	protected void end(){
	}
	public AStarNode findBestPath(boolean[][] walkableGrid, int startRow, int startCol, int goalRow, int goalCol){
		
		AStarNode[][] nodes = new AStarNode[walkableGrid.length][walkableGrid[0].length];
		
		for(int row = 0; row < walkableGrid.length; row++){
			for(int col = 0; col < walkableGrid[row].length; col++){
				nodes[row][col] = new AStarNode(row, col, walkableGrid[row][col]);
			}
		}
		LinkedList<AStarNode> openList = new LinkedList<AStarNode>();
		LinkedList<AStarNode> closedList = new LinkedList<AStarNode>();
		
		nodes[startRow][startCol].costSoFar = 0;
		openList.add(nodes[startRow][startCol]);
		
		while(!openList.isEmpty()){
			AStarNode currentNode = getLowestCostNode(openList, goalRow, goalCol);
			openList.remove(currentNode);
			closedList.add(currentNode);
			
			if(currentNode.col == goalCol && currentNode.row == goalRow){
				return currentNode;
			}
			else{
				int nextStepCost = 1 + currentNode.costSoFar;
				if(currentNode.row - 1 >= 0){
					processNeighbour(currentNode, nodes[currentNode.row - 1][currentNode.col], openList, closedList, nextStepCost);
				}
				
				if(currentNode.row + 1 < nodes.length){
					processNeighbour(currentNode, nodes[currentNode.row + 1][currentNode.col], openList, closedList, nextStepCost);
				}
				
				if(currentNode.col - 1 >= 0){
					processNeighbour(currentNode, nodes[currentNode.row][currentNode.col - 1], openList, closedList, nextStepCost);
				}
				
				if(currentNode.col + 1 < nodes[currentNode.row].length){
					processNeighbour(currentNode, nodes[currentNode.row][currentNode.col + 1], openList, closedList, nextStepCost);
				}
				
			}
		}
		
		return null;
	}
	
	public void processNeighbour(AStarNode parentNode, AStarNode neighbourNode, List<AStarNode> openList, List<AStarNode> closedList, int nextStepCost){
		if(!neighbourNode.accessible){
			return;
		}
		if(!openList.contains(neighbourNode) && !closedList.contains(neighbourNode)){
			neighbourNode.costSoFar = nextStepCost;
			neighbourNode.parentNode = parentNode;
			openList.add(neighbourNode);
		}
		else if(nextStepCost < neighbourNode.costSoFar){
			neighbourNode.costSoFar = nextStepCost;
			neighbourNode.parentNode = parentNode;
		}
	}
	
	public AStarNode getLowestCostNode(List<AStarNode> nodeList, int goalRow, int goalCol){
		int minValue = nodeList.get(0).costSoFar + Math.abs(goalRow - nodeList.get(0).row) + Math.abs(goalCol - nodeList.get(0).col);
		AStarNode minNode = nodeList.get(0);
		for(AStarNode node : nodeList){
			int totalCost = node.costSoFar + Math.abs(goalRow - node.row) + Math.abs(goalCol - node.col);
			if(totalCost < minValue){
				minNode = node;
				minValue = totalCost;
			}
		}
		
		return minNode;
	}

	@Override
	protected void process(Entity e) {
		//Get bomb tile
		GridPos bombGridPos = LevelSystem.getTilePos(e);
		Position bombPos = posMap.get(e);
		Moveable bombMov = moveMap.get(e);
		Owned bombOwner = ownMap.get(e);
		
		//Get enemy tiles
		GroupManager groupManager = world.getManager(GroupManager.class);
		ImmutableBag<Entity> players = groupManager.getEntities(GroupTags.PLAYER);
		//Pick the closest enemy
		Entity closestEnemy = null;
		int closestDist = 0;
		for(int i = 0; i < players.size(); i++){
			GridPos enemyPos = LevelSystem.getTilePos(players.get(i));
			PlayerInfo enemyInf = playerMap.get(players.get(i));
			int metropDistance = Math.abs(enemyPos.r - bombGridPos.r) + Math.abs(enemyPos.c - bombGridPos.c);
			//Set that enemy to the homing target if none are already picked
			if(bombOwner.playerNum != enemyInf.playerNum && (closestEnemy == null || metropDistance < closestDist)){
				closestEnemy = players.get(i);
				closestDist = metropDistance;
			}
		}
		
		if(closestEnemy == null){
			bombMov.ax = 0;
			bombMov.ay = 0;
			return;
		}
		
		//Run A star algorithm
		GridPos closestEnemyPos = LevelSystem.getTilePos(closestEnemy);
		AStarNode bestNode = findBestPath(LevelSystem.accessMap, bombGridPos.r, bombGridPos.c, closestEnemyPos.r, closestEnemyPos.c);
		
		if(bestNode == null){
			bombMov.ax = 0;
			bombMov.ay = 0;
			return;
		}
		
		//Reconstruct path by building up the steps backwards in a list
		//Use debug mode to draw out the path
		AStarNode currentNode = bestNode;
		Array<AStarNode> bestPath = new Array<AStarNode>();
		
		while(currentNode != null){
			bestPath.add(currentNode);
			currentNode = currentNode.parentNode;
		}
		
		AStarNode targetNode;
		if(bestPath.size > 1){
			targetNode = bestPath.get(bestPath.size - 2);
		}
		else{
			targetNode = bestPath.get(0);
		}
		
		Direction targetDirection = null;
		//Find out which direction we are travelling in. Get the current tile pos of the bomb and the grid coordinates of our target and compare their row values.
		//If the row value and column value of both are the same, forget the rest and move directly towards the target.
		if(bombGridPos.r == targetNode.row && bombGridPos.c == targetNode.col){
			bombMov.ax = 0;
			bombMov.ay = 0;
		}
		//If the row value of the target is greater than the current tile, we are heading up
		else if(targetNode.row > bombGridPos.r){
			targetDirection = Direction.UP;
			bombMov.ax = 0;
			bombMov.ay = HOMING_SPEED;
			bombMov.maxVY = HOMING_SPEED;
		}
		//If the row value is less, we are heading down
		else if(targetNode.row < bombGridPos.r){
			targetDirection = Direction.DOWN;
			bombMov.ax = 0;
			bombMov.ay = -HOMING_SPEED;
			bombMov.maxVY = HOMING_SPEED;
		}
		//Else if the column value is greater we are heading right
		else if(targetNode.col > bombGridPos.c){
			targetDirection = Direction.RIGHT;
			bombMov.ax = HOMING_SPEED;
			bombMov.ay = 0;
			bombMov.maxVX = HOMING_SPEED;
		}
		//Else we are heading left
		else{
			targetDirection = Direction.LEFT;
			bombMov.ax = -HOMING_SPEED;
			bombMov.ay = 0;
			bombMov.maxVX = HOMING_SPEED;
		}
		
		//If we are heading left or right, but either our bottom left position is below the position of the current tile our centre is in,
		if(targetDirection == Direction.LEFT || targetDirection == Direction.RIGHT){
			if(bombPos.y < targetNode.row * LevelSystem.tileSize){
				bombMov.ay = ADJUST_SPEED;
				bombMov.maxVY = ADJUST_SPEED;
			}
			else if(bombPos.y + colMap.get(e).height > (targetNode.row + 1) * LevelSystem.tileSize){
				bombMov.ay = -ADJUST_SPEED;
				bombMov.maxVY = ADJUST_SPEED;
			}
		}
		
		//Likewise if we are heading up or down, adjust but swap y for x and height for width
		else if(targetDirection == Direction.UP || targetDirection == Direction.DOWN){
			if(bombPos.x < targetNode.col * LevelSystem.tileSize){
				bombMov.ax = ADJUST_SPEED;
				bombMov.maxVX = ADJUST_SPEED;
			}
			else if(bombPos.x + colMap.get(e).width > (targetNode.col + 1) * LevelSystem.tileSize){
				bombMov.ax = -ADJUST_SPEED;
				bombMov.maxVX = ADJUST_SPEED;
			}
		}
		
	}
}

package com.me.mygdxgame.systems;

import com.artemis.Aspect;
import com.artemis.Entity;
import com.artemis.systems.EntityProcessingSystem;
import com.me.mygdxgame.components.Bomb;

public class BombCollisionSystem extends EntityProcessingSystem {

	@SuppressWarnings("unchecked")
	public BombCollisionSystem() {
		super(Aspect.getAspectForAll(Bomb.class));
	}

	@Override
	protected void process(Entity arg0) {
		// TODO Deal with bouncing of regular tiles and being removed if you fall down a hole
	}

}

package com.me.mygdxgame.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.TimeUtils;
import com.me.mygdxgame.components.Oscillates;
import com.me.mygdxgame.components.Position;
import com.me.mygdxgame.components.Renderable;

public class RenderingSystem extends EntityProcessingSystem {
	@Mapper ComponentMapper<Position> pm;
	@Mapper ComponentMapper<Renderable> rm;
	@Mapper ComponentMapper<Oscillates> om;
	SpriteBatch batch;
	ShapeRenderer shapeRenderer;
	OrthographicCamera camera;
	
	@SuppressWarnings("unchecked")
	public RenderingSystem(SpriteBatch b){
		super(Aspect.getAspectForAll(Position.class, Renderable.class));
		batch = b;
	}
	
	@Override 
	protected void initialize(){
	}
	
	@Override
	protected void begin(){
		batch.begin();
	}
	
	@Override
	protected void end(){
		batch.end();
	}
	@Override
	protected void process(Entity e) {

		Position pos = pm.get(e);
		Renderable renderable = rm.get(e);
		
		float xPos = pos.x;
		float yPos = pos.y;
		
		if(om.has(e)){
			Oscillates oscillation = om.get(e);
			xPos += oscillation.oscillationStrengthX * Math.sin(TimeUtils.millis() / 100);
			yPos += oscillation.oscillationStrengthY * Math.cos(TimeUtils.millis() / 100);
		}

		if(renderable.width == 0 && renderable.height == 0){
			batch.draw(renderable.getImage(), xPos, yPos);
		}
		else{
			batch.draw(renderable.getImage(), xPos, yPos, renderable.width, renderable.height);
		}
	}

}

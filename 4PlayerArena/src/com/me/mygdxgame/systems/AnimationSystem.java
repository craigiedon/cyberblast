package com.me.mygdxgame.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.me.mygdxgame.components.AnimatedRenderable;
import com.me.mygdxgame.components.Position;

public class AnimationSystem extends EntityProcessingSystem{
	@Mapper ComponentMapper<AnimatedRenderable> animMap;
	@Mapper ComponentMapper<Position> posMap;
	SpriteBatch batch;

	@SuppressWarnings("unchecked")
	public AnimationSystem(SpriteBatch gameBatch) {
		super(Aspect.getAspectForAll(AnimatedRenderable.class, Position.class));
		batch = gameBatch;
	}

	@Override
	protected void process(Entity e) {
		AnimatedRenderable animInfo = animMap.get(e);
		Position pos = posMap.get(e);
		animInfo.currentTime += world.getDelta();
		Animation anim = animInfo.getCurrentAnimation();
		
		batch.begin();
		batch.draw(anim.getKeyFrame(animInfo.currentTime), pos.x, pos.y);
		batch.end();
		
	}
}

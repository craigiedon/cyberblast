package com.me.mygdxgame.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import com.artemis.managers.GroupManager;
import com.artemis.systems.EntityProcessingSystem;
import com.artemis.utils.ImmutableBag;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.me.mygdxgame.GroupTags;
import com.me.mygdxgame.XBoxConfig;
import com.me.mygdxgame.components.Charging;
import com.me.mygdxgame.components.Direction;
import com.me.mygdxgame.components.Facing;
import com.me.mygdxgame.components.Inventory;
import com.me.mygdxgame.components.PlayerInfo;
import com.me.mygdxgame.components.Position;
import com.me.mygdxgame.components.Renderable;
import com.me.mygdxgame.components.Moveable;
import com.me.mygdxgame.components.Stunned;

public class MovementSystem extends EntityProcessingSystem implements ControllerListener{
	@Mapper ComponentMapper<Moveable> movMap;
	@Mapper ComponentMapper<Position> pm;
	@Mapper ComponentMapper<Facing> faceMap;
	@Mapper ComponentMapper<Charging> chargeMap;
	@Mapper ComponentMapper<Stunned> stunMap;
	@Mapper ComponentMapper<Inventory> invMap;
	
	private final float normalFriction = 0.8f;
	private final float stunFriction = 0.95f;	
	private final float DEAD_ZONE_THRESHOLD = 0.2f;
	
	@SuppressWarnings("unchecked")
	public MovementSystem() {
		super(Aspect.getAspectForAll(Moveable.class, Position.class));
	}

	@Override
	protected void process(Entity e) {
		Moveable movement = movMap.get(e);
		Position position = pm.get(e);
		Facing playerFacing = faceMap.get(e);
		
		if(stunMap.has(e)){
			movement.vx *= stunFriction;
			movement.vy *= stunFriction;
		}
		else{
			movement.vx *= normalFriction;
			movement.vy *= normalFriction;
		}
		
		if(!stunMap.has(e) && !chargeMap.has(e)){
			
			if(faceMap.has(e) && Math.abs(movement.ax) > 0 && Math.abs(movement.ay) > 0){
				if(movement.ax >= 0 && Math.abs(movement.ax) >= Math.abs(movement.ay)){
					playerFacing.direction = Direction.RIGHT;
				}
				else if(movement.ax < 0 && Math.abs(movement.ax) >= Math.abs(movement.ay)){
					playerFacing.direction = Direction.LEFT;
				}
				else if(movement.ay >= 0 && Math.abs(movement.ay) >= Math.abs(movement.vx)){
					playerFacing.direction = Direction.UP;
				}
				else{
					playerFacing.direction = Direction.DOWN;
				}
			}
			
			//Only apply max velocity ceiling for *user* controls. If an external force pushes it above max then this is allowed
			if(movement.vx <= movement.maxVX && movement.vx >= -movement.maxVX){
				movement.vx += movement.ax;
				if(movement.vx > movement.maxVX){
					movement.vx = movement.maxVX;
				}
				else if(movement.vx < -movement.maxVX){
					movement.vx = -movement.maxVX;
				}
			}

			if(movement.vy <= movement.maxVY && movement.vy >= -movement.maxVY){
				movement.vy += movement.ay;
				if(movement.vy > movement.maxVY){
					movement.vy = movement.maxVY;
				}
				else if(movement.vy < -movement.maxVY){
					movement.vy = -movement.maxVY;
				}
			}
		}
		position.x += movement.vx * Gdx.graphics.getDeltaTime();
		position.y += movement.vy * Gdx.graphics.getDeltaTime();
	}
	
	public static Vector2 getCentre(Entity e){
		Position position = e.getComponent(Position.class);
		Renderable renderable = e.getComponent(Renderable.class);
		
		if(renderable.width == 0 && renderable.height == 0){
			return new Vector2(position.x + (renderable.getImage().getRegionWidth() / 2), position.y + (renderable.getImage().getRegionHeight() / 2));
		}
		else{
			return new Vector2(position.x + renderable.width / 2, position.y + renderable.height / 2);
		}
	}


	@Override
	public boolean axisMoved(Controller controller, int axisNum, float value) {
		ImmutableBag<Entity> players = world.getManager(GroupManager.class).getEntities(GroupTags.PLAYER);
		for(int i = 0; i < players.size(); i++){
			Entity player = players.get(i);
			Moveable playerMoveable = movMap.get(player);
			if(controller.equals(player.getComponent(PlayerInfo.class).controller)){
				float ax = controller.getAxis(XBoxConfig.AXIS_LEFT_X);
				float ay = controller.getAxis(XBoxConfig.AXIS_LEFT_Y);
				Facing playerFacing = faceMap.get(player);

				if((ax * ax + ay * ay) < DEAD_ZONE_THRESHOLD){
					playerMoveable.ax = 0;
					playerMoveable.ay = 0;
				}
				else{
					Inventory inv = invMap.get(player);
					playerMoveable.ax = inv.moveSpeed * ax;
					playerMoveable.ay = -inv.moveSpeed * ay;
					
					playerMoveable.maxVX = Math.abs(playerMoveable.ax);
					playerMoveable.maxVY = Math.abs(playerMoveable.ay);
				}
			}
		}
		return false;
	}

	@Override
	public boolean buttonDown(Controller controller, int buttonCode) {
		return false;
	}

	@Override
	public boolean buttonUp(Controller arg0, int arg1) {
		return false;
	}

	@Override
	public void connected(Controller arg0) {
	}

	@Override
	public void disconnected(Controller arg0) {

	}

	@Override
	public boolean povMoved(Controller arg0, int arg1, PovDirection arg2) {
		return false;
	}

	@Override
	public boolean xSliderMoved(Controller arg0, int arg1, boolean arg2) {
		return false;
	}

	@Override
	public boolean ySliderMoved(Controller arg0, int arg1, boolean arg2) {
		return false;
	}
	
	@Override
	public boolean accelerometerMoved(Controller arg0, int arg1, Vector3 arg2) {
		return false;
	}

}

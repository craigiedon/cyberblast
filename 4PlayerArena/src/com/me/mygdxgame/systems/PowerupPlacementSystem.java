package com.me.mygdxgame.systems;

import java.util.HashMap;

import com.artemis.Entity;
import com.artemis.managers.GroupManager;
import com.artemis.systems.VoidEntitySystem;
import com.artemis.utils.ImmutableBag;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.me.mygdxgame.EntityFactory;
import com.me.mygdxgame.GridPos;
import com.me.mygdxgame.GroupTags;
import com.me.mygdxgame.components.PowerType;

public class PowerupPlacementSystem extends VoidEntitySystem {
	
	final float dropInterval = 2.5f;
	float dropTimer;
	TextureAtlas atlas;
	
	public PowerupPlacementSystem(TextureAtlas atlas){
		dropTimer = dropInterval;
		this.atlas = atlas;
	}
	@Override
	protected void processSystem() {
		dropTimer -= world.getDelta();
		if(dropTimer <= 0){
			dropTimer = dropInterval;
			placePowerup();
		}
	}

	public void placePowerup(){
		boolean[][] accessMap = LevelSystem.accessMap;
		Array<GridPos> potentialPowerupSpots = new Array<GridPos>();
		for(int row = 0; row < accessMap.length; row++){
			for(int col = 0; col < accessMap[row].length; col++){
				if(accessMap[row][col]){
					potentialPowerupSpots.add(new GridPos(row,col));
				}
			}
		}
		
		if(potentialPowerupSpots.size == 0){
			return;
		}
		
		ImmutableBag<Entity> powerUps = world.getManager(GroupManager.class).getEntities(GroupTags.POWERUP);
		Array<GridPos> occupiedSpots = new Array<GridPos>();
		for(int i = 0; i < powerUps.size(); i++){
			occupiedSpots.add(LevelSystem.getTilePos(powerUps.get(i)));
		}
		
		ImmutableBag<Entity> players = world.getManager(GroupManager.class).getEntities(GroupTags.PLAYER);
		for(int i = 0; i < players.size(); i++){
			occupiedSpots.add(LevelSystem.getTilePos(players.get(i)));
		}
		
		int placementTries = 0;
		
		while(placementTries <  100){
			GridPos chosenSpot = potentialPowerupSpots.get(MathUtils.random(0, potentialPowerupSpots.size - 1));
			
			if(occupiedSpots.size == 0){
				EntityFactory.powerupFactory(world, chosenSpot.r, chosenSpot.c, atlas, chooseRandomPower());
				return;
			}
			
			for(GridPos occupiedSpot : occupiedSpots){
				if(!occupiedSpot.equals(chosenSpot)){
					EntityFactory.powerupFactory(world, chosenSpot.r, chosenSpot.c, atlas, chooseRandomPower());
					return;
				}
				else{
					placementTries++;
				}
			}
		}

	}

	public PowerType chooseRandomPower(){
		int randomNum = MathUtils.random(100);
		if(randomNum < 33){
			return PowerType.BOMB_AMOUNT;
		}
		if(randomNum < 66){
			return PowerType.BOMB_SPREAD;
		}
		else{ 
			return PowerType.SPEED_BOOST;
		}
	}

}

package com.me.mygdxgame.systems;

import java.util.ArrayList;
import java.util.HashMap;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.World;
import com.artemis.annotations.Mapper;
import com.artemis.managers.GroupManager;
import com.artemis.systems.EntityProcessingSystem;
import com.artemis.utils.ImmutableBag;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.me.mygdxgame.BombBehaviour;
import com.me.mygdxgame.CharacterType;
import com.me.mygdxgame.EntityFactory;
import com.me.mygdxgame.GridPos;
import com.me.mygdxgame.GroupTags;
import com.me.mygdxgame.XBoxConfig;
import com.me.mygdxgame.components.Bomb;
import com.me.mygdxgame.components.Collidable;
import com.me.mygdxgame.components.Destructable;
import com.me.mygdxgame.components.Direction;
import com.me.mygdxgame.components.Expires;
import com.me.mygdxgame.components.Inventory;
import com.me.mygdxgame.components.Owned;
import com.me.mygdxgame.components.PlayerInfo;
import com.me.mygdxgame.components.Position;
import com.me.mygdxgame.components.Renderable;
import com.me.mygdxgame.components.Moveable;

public class BombSystem extends EntityProcessingSystem implements ControllerListener{

	@Mapper ComponentMapper<Bomb> bombMap;
	@Mapper ComponentMapper<Destructable> destMap;
	@Mapper static ComponentMapper<PlayerInfo> playerMap;
	@Mapper static ComponentMapper<Inventory> invMap;
	@Mapper static ComponentMapper<Owned> ownMap;
	TextureAtlas atlas;
	ArrayList<Integer> playerDetonations;
	HashMap<CharacterType, BombBehaviour> playerBombBehaviour;
	public static HashMap<BombBehaviour, String> bombImages; 
	private final float normalBombTime = 2;
	private final float normalExplosionFlameTime = 0.25f;
	private final float napalmExplosionFlameTime = 1;
	
	@SuppressWarnings("unchecked")
	public BombSystem(TextureAtlas atlas) {
		super(Aspect.getAspectForAll(Bomb.class));
		this.atlas = atlas;
		playerDetonations = new ArrayList<Integer>();
		
		playerBombBehaviour = new HashMap<CharacterType, BombBehaviour>();
		playerBombBehaviour.put(CharacterType.NORMAL, BombBehaviour.NORMAL);
		playerBombBehaviour.put(CharacterType.NAPALM, BombBehaviour.NAPALM);
		playerBombBehaviour.put(CharacterType.FORCE, BombBehaviour.NORMAL);
		playerBombBehaviour.put(CharacterType.REMOTE, BombBehaviour.REMOTE);
		playerBombBehaviour.put(CharacterType.HOMING, BombBehaviour.HOMING);
		
		bombImages = new HashMap<BombBehaviour, String>();
		bombImages.put(BombBehaviour.HOMING, "thermalBombHoming");
		bombImages.put(BombBehaviour.NAPALM, "thermalBombNapalm");
		bombImages.put(BombBehaviour.NORMAL, "thermalBomb");
		bombImages.put(BombBehaviour.REMOTE, "thermalBombRemote");
	}

	@Override
	protected void process(Entity e) {
		Bomb bomb = bombMap.get(e);
		Owned owner = ownMap.get(e);
		
		
		//Remote Bombs
		if(playerDetonations.contains(owner.playerNum)){
			bomb.expiryTime = 0;
		}
		else{
			bomb.expiryTime -= world.getDelta();
		}
		
		if(bomb.expiryTime <= 0){
			if(bomb.bombBehaviour == BombBehaviour.NAPALM){
				explode(e, napalmExplosionFlameTime, bomb.spread);
			}
			else{
				explode(e, normalExplosionFlameTime, bomb.spread);
			}
			
			//When a player's bomb explodes, replenish the amount of bombs they have available to drop
			replenishBomb(e, world);
		}
		

	}
	
	@Override
	protected void end(){
		playerDetonations.clear();
	}
	
	protected void explode(Entity e, float burnTime, int spreadAmount){
		Entity explosion = world.createEntity();
		
		explosion.addComponent(new Position());
		Renderable explosionRenderable = new Renderable(atlas.findRegion("explosionCentre"));
		explosion.addComponent(explosionRenderable);
		explosion.addComponent(new Collidable(explosionRenderable.width, explosionRenderable.height));
		explosion.addComponent(new Expires(burnTime));
		GridPos bombTile = LevelSystem.getTilePos(e);
		LevelSystem.placeAtTile(explosion, bombTile.r, bombTile.c);

		explosion.addToWorld();
		GroupManager groupManager = world.getManager(GroupManager.class);
		groupManager.add(explosion, "Explosion");
		
		spreadExplosion(bombTile, Direction.UP, spreadAmount, burnTime);
		spreadExplosion(bombTile, Direction.DOWN, spreadAmount, burnTime);
		spreadExplosion(bombTile, Direction.LEFT, spreadAmount, burnTime);
		spreadExplosion(bombTile, Direction.RIGHT, spreadAmount, burnTime);

		e.deleteFromWorld();
	}
	
	protected void spreadExplosion(GridPos currentTile, Direction dir, int spreadNum, float burnTime){
		if(spreadNum > 0){
			GroupManager groupManager = world.getManager(GroupManager.class);
			ImmutableBag<Entity> blocks = groupManager.getEntities("Block");
			
			GridPos newSpreadTile;
			Renderable explosionRenderable;

			switch(dir){
			case UP:
				newSpreadTile = new GridPos(currentTile.r + 1, currentTile.c);
				explosionRenderable = new Renderable(atlas.findRegion("explosionVertical"));
				break;
			case LEFT:
				newSpreadTile = new GridPos(currentTile.r, currentTile.c - 1);
				explosionRenderable = new Renderable(atlas.findRegion("explosionHorizontal"));
				break;
			case DOWN:
				newSpreadTile = new GridPos(currentTile.r - 1, currentTile.c);
				explosionRenderable = new Renderable(atlas.findRegion("explosionVertical"));
				break;
			case RIGHT:
				newSpreadTile = new GridPos(currentTile.r, currentTile.c + 1);
				explosionRenderable = new Renderable(atlas.findRegion("explosionHorizontal"));
				break;
			default:
				newSpreadTile = new GridPos(currentTile.r + 1, currentTile.c);
				explosionRenderable = new Renderable(atlas.findRegion("explosionVertical"));
			}
			
			int newSpreadNum = spreadNum - 1;
			
			boolean blockHit = false;
			for(int i = 0; i < blocks.size(); i++){
				Entity block = blocks.get(i);
				GridPos blockTile = LevelSystem.getTilePos(block);
				if(blockTile.c == newSpreadTile.c && blockTile.r == newSpreadTile.r){
					newSpreadNum = 0;
					if(destMap.has(block)){
						block.deleteFromWorld();
					}
					else{
						blockHit = true;
					}
					break;
				}
			}
			
			if(!blockHit){
				Entity newSpread = world.createEntity();
				newSpread.addComponent(new Position());
				newSpread.addComponent(explosionRenderable);
				newSpread.addComponent(new Collidable(explosionRenderable.width, explosionRenderable.height));
				newSpread.addComponent(new Expires(burnTime));

				LevelSystem.placeAtTile(newSpread, newSpreadTile.r, newSpreadTile.c);
				newSpread.addToWorld();

				groupManager.add(newSpread, "Explosion");
				spreadExplosion(newSpreadTile, dir, newSpreadNum, burnTime);
			}
		}
	}
	
	public static void replenishBomb(Entity e, World w){
		ImmutableBag<Entity> players = w.getManager(GroupManager.class).getEntities(GroupTags.PLAYER);
		for(int i = 0; i < players.size(); i++){
			Entity player = players.get(i);
			PlayerInfo pInf = playerMap.get(player);
			if(ownMap.get(e).playerNum == pInf.playerNum){
				invMap.get(player).bombsLeft += 1;
			}
		}
	}

	@Override
	public boolean buttonDown(Controller controller, int buttonCode) {
		ImmutableBag<Entity> players = world.getManager(GroupManager.class).getEntities(GroupTags.PLAYER);
		for(int i = 0; i < players.size(); i++){
			
			PlayerInfo playerInf = playerMap.get(players.get(i));
			Inventory inv = invMap.get(players.get(i));
			Inventory bombBag = invMap.get(players.get(i));
			Controller playerController = playerInf.controller;
			
			if(playerController.equals(controller)){
				
				//Regular bomb placement
				if(buttonCode == XBoxConfig.BUTTON_A){
					if(bombBag.bombsLeft > 0){
						bombBag.bombsLeft -= 1;
						Vector2 playerCentre = MovementSystem.getCentre(players.get(i));
						EntityFactory.bombFactory(world, playerCentre.x, playerCentre.y , atlas, playerBombBehaviour.get(playerInf.charType), playerInf.playerNum, normalBombTime, inv.spreadingPower);
					}
				}
				
				//Use special power
				if(buttonCode == XBoxConfig.BUTTON_B){
					if(playerInf.charType == CharacterType.REMOTE){
						playerDetonations.add(playerInf.playerNum);
					}
				}
			}
		}
		return false;
	}
	
	@Override
	public boolean buttonUp(Controller arg0, int arg1) {
		return false;
	}

	@Override
	public void connected(Controller arg0) {
	}

	@Override
	public void disconnected(Controller arg0) {
	}

	@Override
	public boolean povMoved(Controller arg0, int arg1, PovDirection arg2) {
		return false;
	}

	@Override
	public boolean xSliderMoved(Controller arg0, int arg1, boolean arg2) {
		return false;
	}

	@Override
	public boolean ySliderMoved(Controller arg0, int arg1, boolean arg2) {
		return false;
	}
	
	@Override
	public boolean accelerometerMoved(Controller arg0, int arg1, Vector3 arg2) {
		return false;
	}

	@Override
	public boolean axisMoved(Controller arg0, int arg1, float arg2) {
		return false;
	}

}

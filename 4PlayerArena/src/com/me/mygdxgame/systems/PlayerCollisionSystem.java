package com.me.mygdxgame.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import com.artemis.managers.GroupManager;
import com.artemis.systems.EntityProcessingSystem;
import com.artemis.utils.ImmutableBag;
import com.badlogic.gdx.math.Rectangle;
import com.me.mygdxgame.GridPos;
import com.me.mygdxgame.GroupTags;
import com.me.mygdxgame.components.Bomb;
import com.me.mygdxgame.components.Collidable;
import com.me.mygdxgame.components.Inventory;
import com.me.mygdxgame.components.Moveable;
import com.me.mygdxgame.components.PlayerInfo;
import com.me.mygdxgame.components.Position;
import com.me.mygdxgame.components.Powerup;

public class PlayerCollisionSystem extends EntityProcessingSystem{
	@Mapper ComponentMapper<Position> pm;
	@Mapper ComponentMapper<Collidable> cm;
	@Mapper ComponentMapper<Bomb> bombMap;
	@Mapper ComponentMapper<Moveable> movMap;
	@Mapper ComponentMapper<PlayerInfo> playerMap;
	@Mapper ComponentMapper<Powerup> powerMap;
	@Mapper ComponentMapper<Inventory> invMap;
	
	@SuppressWarnings("unchecked")
	public PlayerCollisionSystem(){
		super(Aspect.getAspectForAll(Moveable.class, Position.class, Collidable.class));
	}

	@Override
	protected void process(Entity e) {
		GroupManager groupManager = world.getManager(GroupManager.class);
		ImmutableBag<Entity> blocks = groupManager.getEntities(GroupTags.BLOCK);
		ImmutableBag<Entity> explosions = groupManager.getEntities(GroupTags.EXPLOSION);
		ImmutableBag<Entity> holes = groupManager.getEntities(GroupTags.HOLE);
		ImmutableBag<Entity> powerups = groupManager.getEntities(GroupTags.POWERUP);
		
		Position position = pm.get(e);
		Collidable collidable = cm.get(e);
		
		//Block collision code
		for(int i = 0; i < blocks.size(); i++){
			Rectangle collisionRect = new Rectangle(position.x, position.y, collidable.width, collidable.height);
			Entity block = blocks.get(i);
			Position blockPos = pm.get(block);
			Collidable blockCollidable = cm.get(block);
			Rectangle blockSurface = new Rectangle(blockPos.x, blockPos.y, blockCollidable.width, blockCollidable.height);
				
			//Separate with rectangle rectangle AABB collision
			boolean isOverlappingX = false;
			boolean isOverlappingY = false;
			float overlapX = 0;
			float overlapY = 0;
			
			if(collisionRect.x < blockSurface.x && blockSurface.x < collisionRect.x + collisionRect.width){
				//Could be a left collision
				isOverlappingX = true;
				overlapX = collisionRect.x + collisionRect.width - blockSurface.x;
			}
			else if(collisionRect.x >= blockSurface.x && collisionRect.x < blockSurface.x + blockSurface.width){
				//Could be a right collision
				isOverlappingX = true;
				overlapX = collisionRect.x - (blockSurface.x + blockSurface.width);
			}
			
			if(collisionRect.y < blockSurface.y && blockSurface.y < collisionRect.y + collisionRect.height){
				//Could be a bottom collision
				isOverlappingY = true;
				overlapY = collisionRect.y + collisionRect.height - blockSurface.y;
			}
			else if(collisionRect.y >= blockSurface.y && collisionRect.y < blockSurface.y + blockSurface.height){
				//Could be a top collision
				isOverlappingY = true;
				overlapY = collisionRect.y - (blockSurface.y + blockSurface.height);
			}
			
			
			Moveable playerMov = movMap.get(e);
			if(isOverlappingX && isOverlappingY){
				if(Math.abs(overlapX) < Math.abs(overlapY)){
					position.x -= overlapX;
					playerMov.vx *= -1;
				}
				else{
					position.y -= overlapY;
					playerMov.vy *= -1;
				}
			}
		}
				
		//Holes collision code
		for(int i = 0; i < holes.size(); i++){
			GridPos holeTilePos = LevelSystem.getTilePos(holes.get(i));
			GridPos playerTilePos = LevelSystem.getTilePos(e);
			
			if((holeTilePos.r == playerTilePos.r) && (holeTilePos.c == playerTilePos.c)){
				e.deleteFromWorld();
				if(bombMap.has(e)){
					BombSystem.replenishBomb(e, world);
				}
			}
		}
		
		
		//Explosion collision code
		if(playerMap.has(e)){
			for(int i = 0; i < explosions.size(); i++){
				Entity explosion = explosions.get(i);
				Position explosionPos = pm.get(explosion);
				Collidable explosionDims = cm.get(explosion);
				
				Rectangle playerCollisionSurface = new Rectangle(position.x, position.y, collidable.width, collidable.height);
				Rectangle explosionCollisionSurface = new Rectangle(explosionPos.x, explosionPos.y, explosionDims.width, explosionDims.height);
				
				if(explosionCollisionSurface.overlaps(playerCollisionSurface)){
					e.deleteFromWorld();
				}	
			}
			
			for(int i = 0; i < powerups.size(); i++){
				Entity powerup = powerups.get(i);
				Position powerupPos = pm.get(powerup);
				Collidable powerupDims = cm.get(powerup);
				
				Rectangle playerCollisionSurface = new Rectangle(position.x, position.y, collidable.width, collidable.height);
				Rectangle powerupCollisionSurface = new Rectangle(powerupPos.x, powerupPos.y, powerupDims.width, powerupDims.height);
				
				if(powerupCollisionSurface.overlaps(playerCollisionSurface)){
					powerupPlayer(e, powerMap.get(powerup));
					powerup.deleteFromWorld();
				}
			}
		}
	}

	private void powerupPlayer(Entity player, Powerup powerup) {
		Inventory inv = invMap.get(player);
		Moveable playerMov = movMap.get(player);
		switch(powerup.powerType){
		case SPEED_BOOST:
			if(inv.moveSpeed < 400){
				inv.moveSpeed += 20;
			}
			break;
		case BOMB_SPREAD:
			inv.spreadingPower += 1;
			break;
		case BOMB_AMOUNT:
			inv.bombsLeft += 1;
			break;
		}
	}
}

package com.me.mygdxgame.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.me.mygdxgame.components.Position;
import com.me.mygdxgame.components.TextRenderable;

public class TextRenderingSystem extends EntityProcessingSystem {
	@Mapper ComponentMapper<Position> posMap;
	@Mapper ComponentMapper<TextRenderable> txtMap;
	
	OrthographicCamera camera;
	SpriteBatch batch;
	
	@SuppressWarnings("unchecked")
	public TextRenderingSystem(OrthographicCamera camera) {
		super(Aspect.getAspectForAll(Position.class, TextRenderable.class));
		batch = new SpriteBatch();
		this.camera = camera;
	}
	
	@Override
	protected void initialize(){
		
	}
	
	@Override
	protected void begin(){
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
	}
	
	@Override
	protected void end(){
		batch.end();
	}

	@Override
	protected void process(Entity e) {
		Position position = posMap.get(e);
		TextRenderable txtRend = txtMap.get(e);
		BitmapFont font = txtRend.font;
		
		if(txtRend.centred){
			TextBounds textBounds = txtRend.getDimensions();
			font.draw(batch, txtRend.text, position.x - textBounds.width / 2, position.y + textBounds.height / 2);
		}
		else{
			font.draw(batch, txtRend.text, position.x, position.y);
		}
	}

}

package com.me.mygdxgame.systems;

import com.artemis.ComponentMapper;
import com.artemis.managers.*;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import com.artemis.systems.VoidEntitySystem;
import com.artemis.utils.ImmutableBag;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.me.mygdxgame.CharacterType;
import com.me.mygdxgame.EntityFactory;
import com.me.mygdxgame.GridPos;
import com.me.mygdxgame.GroupTags;
import com.me.mygdxgame.LevelInfo;
import com.me.mygdxgame.MyGdxGame;
import com.me.mygdxgame.TileTag;
import com.me.mygdxgame.WinScreen;
import com.me.mygdxgame.XBoxConfig;
import com.me.mygdxgame.components.Collidable;
import com.me.mygdxgame.components.Destructable;
import com.me.mygdxgame.components.Hole;
import com.me.mygdxgame.components.PlayerInfo;
import com.me.mygdxgame.components.Position;
import com.me.mygdxgame.components.Renderable;

public class LevelSystem extends VoidEntitySystem implements ControllerListener {
	@Mapper ComponentMapper<PlayerInfo> playerMap;
	@Mapper static ComponentMapper<Position> posMap;
	@Mapper static ComponentMapper<Renderable> rendMap;
	GameState currentState = GameState.RUNNING;

	public LevelInfo originalLevelInfo;
	TileTag[][] levelTiles;
	public static int tileSize = 45;
	int nRows;
	int nCols;
	TextureAtlas atlas;
	public Array<PlayerInfo> participatingPlayers;
	public static boolean[][] accessMap;
	MyGdxGame game;
	float winTimer;
	public PlayerInfo winningPlayer;

	public LevelSystem(LevelInfo levelInfo , Array<PlayerInfo> participatingPlayers, TextureAtlas atlas, MyGdxGame g) {
		originalLevelInfo = levelInfo;
		levelTiles = levelInfo.levelTiles;
		this.participatingPlayers = participatingPlayers;
		game = g;
		nRows = levelTiles.length;
		nCols = levelTiles[0].length;
		this.atlas = atlas;
		accessMap = new boolean[nRows][nCols];
		
		//Initially we believe all grid places to be accessible
		for(int i = 0; i < accessMap.length; i++){
			for(int j = 0; j < accessMap[i].length; j++){
				accessMap[i][j] = true;
			}
		}
		
	}
	
	@Override
	protected void initialize(){
		
		//Create level tiles
		for(int row = 0; row < levelTiles.length; row++){
			for(int col = 0; col < levelTiles[row].length; col++){
				EntityFactory.floorFactory(world, row, col, atlas);
			}
		}

		for(int row = 0; row < levelTiles.length; row++){
			for(int col = 0; col < levelTiles[row].length; col++){
				if(levelTiles[row][col] == TileTag.INDESTRUCTIBLE_BLOCK){
					Entity block = world.createEntity();
					block.addComponent(new Position(col * tileSize, row * tileSize));
					block.addComponent(new Renderable(atlas.findRegion("indestructibleTile"), tileSize, tileSize));
					block.addComponent(new Collidable(tileSize,tileSize));
					block.addToWorld();
					world.getManager(GroupManager.class).add(block, "Block");
				}
				if(levelTiles[row][col] == TileTag.DESTRUCTIBLE_BLOCK){
					Entity block = world.createEntity();
					block.addComponent(new Position(col * tileSize, row * tileSize));
					block.addComponent(new Renderable(atlas.findRegion("destructibleTile"), tileSize, tileSize));
					block.addComponent(new Collidable(tileSize,tileSize));
					block.addComponent(new Destructable());
					block.addToWorld();
					world.getManager(GroupManager.class).add(block, "Block");
				}
				if(levelTiles[row][col] == TileTag.HOLE){
					EntityFactory.holeFactory(world, row, col, atlas);
				}
				if(levelTiles[row][col] == TileTag.P1_START){
					for(PlayerInfo player : participatingPlayers){
						if(player.playerNum == 1){
							EntityFactory.playerFactory(world, player.playerNum, player.controller, player.charType, col * tileSize, row * tileSize, atlas);
						}
					}

				}
				if(levelTiles[row][col] == TileTag.P2_START){
					for(PlayerInfo player : participatingPlayers){
						if(player.playerNum == 2){
							EntityFactory.playerFactory(world, player.playerNum, player.controller, player.charType, col * tileSize, row * tileSize, atlas);
						}
					}
				}
				if(levelTiles[row][col] == TileTag.P3_START){
					for(PlayerInfo player : participatingPlayers){
						if(player.playerNum == 3){
							EntityFactory.playerFactory(world, player.playerNum, player.controller, player.charType, col * tileSize, row * tileSize, atlas);
						}
					}
				}
				if(levelTiles[row][col] == TileTag.P4_START){
					for(PlayerInfo player : participatingPlayers){
						if(player.playerNum == 4){
							EntityFactory.playerFactory(world, player.playerNum, player.controller, player.charType, col * tileSize, row * tileSize, atlas);
						}
					}
				}
			}
		}
	}

	@Override
	protected void processSystem() {
		GroupManager groupManager = world.getManager(GroupManager.class);
		ImmutableBag<Entity> players = groupManager.getEntities(GroupTags.PLAYER);
		ImmutableBag<Entity> blocks = groupManager.getEntities(GroupTags.BLOCK);
		ImmutableBag<Entity> holes = groupManager.getEntities(GroupTags.HOLE);
		
		for(int row = 0; row < accessMap.length; row++){
			for(int col = 0; col < accessMap[row].length; col++){
				accessMap[row][col] = true;
			}
		}
		
		for(int i = 0; i < blocks.size(); i++){
			GridPos blockTile = getTilePos(blocks.get(i));
			accessMap[blockTile.r][blockTile.c] = false;
		}
		for(int i = 0; i < holes.size(); i++){
			GridPos holeTile = getTilePos(holes.get(i));
			accessMap[holeTile.r][holeTile.c] = false;
		}
		
		if(currentState == GameState.RUNNING){
			if(players.size() == 1){
				System.out.println("Winner");
				currentState = GameState.WON;
				winningPlayer = playerMap.get(players.get(0));
				winTimer = 3;
			}
			else if(players.size() == 0){
				System.out.println("Draw?");
				currentState = GameState.WON;
				winningPlayer = null;
				winTimer = 3;
			}
		}
		else if(currentState == GameState.WON){
			winTimer -= world.getDelta();
		}
	}
	
	public boolean isGameOver(){
		return (currentState == GameState.WON) && (winTimer <= 0); 
	}
	
	public static void placeAtTile(Entity e, int row, int column){
			Position position = posMap.get(e);
			Renderable renderable = rendMap.get(e);
			position.x = column * tileSize + (tileSize / 2) - renderable.width / 2;
			position.y = row * tileSize + (tileSize / 2) - renderable.height / 2;
	}
	
	public static GridPos getTilePos(Entity e){
			Vector2 centrePos = MovementSystem.getCentre(e);
			return new GridPos((int) (centrePos.y / tileSize), (int) (centrePos.x / tileSize));
	}


	public Vector2 calculateLevelDimensions(){
		int width = levelTiles[0].length * tileSize;
		int height = levelTiles.length * tileSize;
		return new Vector2(width, height);
	}
	
	
	
	@Override
	public boolean buttonDown(Controller controller, int buttonCode) {
		return false;
	}

	@Override
	public boolean buttonUp(Controller arg0, int arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void connected(Controller arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disconnected(Controller arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean povMoved(Controller arg0, int arg1, PovDirection arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean xSliderMoved(Controller arg0, int arg1, boolean arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean ySliderMoved(Controller arg0, int arg1, boolean arg2) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	@Override
	public boolean accelerometerMoved(Controller arg0, int arg1, Vector3 arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean axisMoved(Controller arg0, int arg1, float arg2) {
		// TODO Auto-generated method stub
		return false;
	}
}

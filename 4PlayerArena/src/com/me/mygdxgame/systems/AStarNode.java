package com.me.mygdxgame.systems;

public class AStarNode{
	int row;
	int col;
	int costSoFar;
	AStarNode parentNode;
	final int UNKNOWN_COST = -1;
	boolean accessible;
	
	public AStarNode(int r, int c, boolean accessValue){
		row = r;
		col = c;
		costSoFar = UNKNOWN_COST;
		parentNode = null;
		accessible = accessValue;
	}
	
	public Integer getTotalCost(int goalRow, int goalCol){
		int estimatedCost = Math.abs(goalRow - row) + Math.abs(goalCol - col);
		return new Integer(estimatedCost + costSoFar);
	}
}

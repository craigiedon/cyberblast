package com.me.mygdxgame.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import com.artemis.managers.GroupManager;
import com.artemis.systems.EntityProcessingSystem;
import com.artemis.utils.ImmutableBag;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.me.mygdxgame.CharacterType;
import com.me.mygdxgame.GroupTags;
import com.me.mygdxgame.XBoxConfig;
import com.me.mygdxgame.components.AnimatedRenderable;
import com.me.mygdxgame.components.AnimationTag;
import com.me.mygdxgame.components.Charging;
import com.me.mygdxgame.components.Collidable;
import com.me.mygdxgame.components.Direction;
import com.me.mygdxgame.components.Expires;
import com.me.mygdxgame.components.Facing;
import com.me.mygdxgame.components.Moveable;
import com.me.mygdxgame.components.PlayerInfo;
import com.me.mygdxgame.components.Position;
import com.me.mygdxgame.components.Renderable;
import com.me.mygdxgame.components.Stunned;

public class ForceChargingSystem extends EntityProcessingSystem implements ControllerListener{

	@Mapper ComponentMapper<PlayerInfo> playerMap;
	@Mapper ComponentMapper<Charging> chargeMap;
	@Mapper ComponentMapper<Moveable> moveMap;
	@Mapper ComponentMapper<Facing> faceMap;
	@Mapper ComponentMapper<Renderable> rendMap;
	@Mapper ComponentMapper<Position> posMap;
	@Mapper ComponentMapper<Collidable> colMap;
	TextureAtlas atlas;
	
	Animation forceBlastDownAnimation;
	Animation forceBlastRightAnimation;
	Animation forceBlastLeftAnimation;
	Animation forceBlastUpAnimation;
	
	private final int IMPULSE_FORCE = 500;
	
	@SuppressWarnings("unchecked")
	public ForceChargingSystem(TextureAtlas atlas) {
		super(Aspect.getAspectForAll(PlayerInfo.class, Charging.class, Moveable.class, Facing.class, Renderable.class, Position.class));
		this.atlas = atlas;
		
		//Down
		Array<TextureRegion> forceBlastDownFrames = new Array<TextureRegion>();
		for(int i = 1; i < 10; i++){
			forceBlastDownFrames.add(atlas.findRegion("forceBlastDown000" + i));
		}
		for(int i = 10; i < 12; i++){
			forceBlastDownFrames.add(atlas.findRegion("forceBlastDown00" + i));
		}
		forceBlastDownAnimation = new Animation(0.03f, forceBlastDownFrames);
		
		//Right
		Array<TextureRegion> forceBlastRightFrames = new Array<TextureRegion>();
		for(int i = 1; i < 10; i++){
			forceBlastRightFrames.add(atlas.findRegion("forceBlastRight000" + i));
		}
		for(int i = 10; i < 12; i++){
			forceBlastRightFrames.add(atlas.findRegion("forceBlastRight00" + i));
		}
		forceBlastRightAnimation = new Animation(0.03f, forceBlastRightFrames);
		
		//Left
		Array<TextureRegion> forceBlastLeftFrames = new Array<TextureRegion>();
		for(int i = 1; i < 10; i++){
			forceBlastLeftFrames.add(atlas.findRegion("forceBlastLeft000" + i));
		}
		for(int i = 10; i < 12; i++){
			forceBlastLeftFrames.add(atlas.findRegion("forceBlastLeft00" + i));
		}
		forceBlastLeftAnimation = new Animation(0.03f, forceBlastLeftFrames);
		
		//Up
		Array<TextureRegion> forceBlastUpFrames = new Array<TextureRegion>();
		for(int i = 1; i < 10; i++){
			forceBlastUpFrames.add(atlas.findRegion("forceBlastUp000" + i));
		}
		for(int i = 10; i < 12; i++){
			forceBlastUpFrames.add(atlas.findRegion("forceBlastUp00" + i));
		}
		forceBlastUpAnimation = new Animation(0.03f, forceBlastUpFrames);
	}

	@Override
	protected void process(Entity e) {
		Charging charging = chargeMap.get(e);
		charging.chargeTimeLeft -= world.getDelta();
		if(charging.chargeTimeLeft <= 0){
			e.removeComponent(Charging.class);
			e.addToWorld();
			
			Entity forceBlast = world.createEntity();
			forceBlast.addComponent(new Expires(0.36f));
			
			Facing playerFacing = faceMap.get(e);
			Renderable playerRenderable = rendMap.get(e);
			Position playerPos = posMap.get(e);
			Position forcePos = new Position();
			int impulseX = 0;
			int impulseY = 0;
			
			AnimatedRenderable forceRend = null;
			
			switch(playerFacing.direction){
			case LEFT:
				forcePos = new Position(playerPos.x - 35, playerPos.y);
				impulseX = -IMPULSE_FORCE;
				forceRend = new AnimatedRenderable(AnimationTag.FORCE_BLAST_DOWN, forceBlastLeftAnimation);
				break;
			case RIGHT:
				forcePos = new Position(playerPos.x + playerRenderable.width, playerPos.y);
				impulseX = IMPULSE_FORCE;
				forceRend = new AnimatedRenderable(AnimationTag.FORCE_BLAST_DOWN, forceBlastRightAnimation);
				break;
			case UP:
				forcePos = new Position(playerPos.x, playerPos.y + playerRenderable.height);
				impulseY = IMPULSE_FORCE;
				forceRend = new AnimatedRenderable(AnimationTag.FORCE_BLAST_DOWN, forceBlastUpAnimation);
				break;
			case DOWN:
				forcePos = new Position(playerPos.x, playerPos.y - 35);
				impulseY = -IMPULSE_FORCE;
				forceRend = new AnimatedRenderable(AnimationTag.FORCE_BLAST_DOWN, forceBlastDownAnimation);
				break;
			}
			
			forceBlast.addComponent(forceRend);
			forceBlast.addComponent(forcePos);
			forceBlast.addToWorld();
			
			Rectangle blastRect = new Rectangle(forcePos.x, forcePos.y, 35, 35);
			
			GroupManager groupManager = world.getManager(GroupManager.class);
			ImmutableBag<Entity> players = groupManager.getEntities(GroupTags.PLAYER);
			for(int i = 0; i < players.size(); i++){
				Entity player = players.get(i);
				Position pos = posMap.get(player);
				Collidable col = colMap.get(player); 
				Rectangle playerRect = new Rectangle(pos.x, pos.y, col.width, col.height);
				if(playerRect.overlaps(blastRect)){
					Moveable playerMoveable = moveMap.get(player);
					playerMoveable.vx = impulseX;
					playerMoveable.vy = impulseY;
					player.addComponent(new Stunned(0.5f));
					player.addToWorld();
				}
			}
			
			ImmutableBag<Entity> bombs = groupManager.getEntities(GroupTags.BOMB);
			for(int i = 0; i < bombs.size(); i++){
				Entity bomb = bombs.get(i);
				Position pos = posMap.get(bomb);
				Collidable col = colMap.get(bomb);
				Rectangle bombRect = new Rectangle(pos.x, pos.y, col.width, col.height);
				if(bombRect.overlaps(blastRect)){
					Moveable bombMoveable = moveMap.get(bomb);
					bombMoveable.vx = impulseX;
					bombMoveable.vy = impulseY;
					bomb.addComponent(new Stunned(0.5f));
					bomb.addToWorld();
				}
			}
			
			
		}
	}



	@Override
	public boolean buttonDown(Controller controller, int buttonCode) {
		ImmutableBag<Entity> players = world.getManager(GroupManager.class).getEntities(GroupTags.PLAYER);
		for(int i = 0; i < players.size(); i++){
			Entity player = players.get(i);
			PlayerInfo playerInfo = playerMap.get(player);
			Moveable movement = moveMap.get(player);
			if(controller.equals(playerInfo.controller) && playerInfo.charType == CharacterType.FORCE && buttonCode == XBoxConfig.BUTTON_B){
				player.addComponent(new Charging(0.25f));
				movement.ax = 0;
				movement.ay = 0;
				player.addToWorld();
			}
		}
		return false;
	}

	@Override
	public boolean buttonUp(Controller arg0, int arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void connected(Controller arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disconnected(Controller arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean povMoved(Controller arg0, int arg1, PovDirection arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean xSliderMoved(Controller arg0, int arg1, boolean arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean ySliderMoved(Controller arg0, int arg1, boolean arg2) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean accelerometerMoved(Controller arg0, int arg1, Vector3 arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean axisMoved(Controller arg0, int arg1, float arg2) {
		// TODO Auto-generated method stub
		return false;
	}
	
}

package com.me.mygdxgame;

import com.artemis.Entity;
import com.artemis.World;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Array;
import com.me.mygdxgame.components.PlayerInfo;
import com.me.mygdxgame.components.Position;
import com.me.mygdxgame.components.TextRenderable;
import com.me.mygdxgame.systems.RenderingSystem;
import com.me.mygdxgame.systems.TextRenderingSystem;

public class PlayerSelectScreen implements Screen {
	MyGdxGame game;
	TextureAtlas atlas;
	OrthographicCamera camera;
	Array<PlayerSelectBox> playerSelectBoxes;
	World world;
	BitmapFont videoGameFont32;
	BitmapFont videoGameFont16;
	
	public PlayerSelectScreen(MyGdxGame g){
		game = g;
		atlas = new TextureAtlas(Gdx.files.internal("placeHolderGraphics.atlas"));
		camera = new OrthographicCamera();
		camera.setToOrtho(false);
		playerSelectBoxes = new Array<PlayerSelectBox>();
		videoGameFont32 = game.videoGameFontGenerator.generateFont(32);
		videoGameFont32.setColor(Color.WHITE);
		videoGameFont16 = game.videoGameFontGenerator.generateFont(16);
		videoGameFont16.setColor(Color.WHITE);
		
				
		world = new World();
		world.setSystem(new RenderingSystem(game.batch));
		world.setSystem(new TextRenderingSystem(camera));
		world.initialize();
		
		Array<Controller> controllers = Controllers.getControllers();
		for(int i = 0; i < 4 && i < controllers.size; i++){
			PlayerSelectBox playerSelectBox= new PlayerSelectBox(world, i + 1, atlas, controllers.get(i), videoGameFont16);
			playerSelectBoxes.add(playerSelectBox);
			 controllers.get(i).addListener(playerSelectBox);
		}
		
		Entity titleText = world.createEntity();
		titleText.addComponent(new Position(camera.viewportWidth / 2 , camera.viewportHeight - 30));
		titleText.addComponent(new TextRenderable("Select Players", videoGameFont32 ,true));
		titleText.addToWorld();
	}

	@Override
	public void render(float arg0) {
		Gdx.gl.glClearColor(0,0,0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		camera.update();
		game.batch.setProjectionMatrix(camera.combined);
		
		world.setDelta(Gdx.graphics.getDeltaTime());
		world.process();
		
		for(PlayerSelectBox player : playerSelectBoxes){
			if(player.currentState == PlayerSelectBoxState.START_REQUESTED){
				Array<PlayerInfo> participatingPlayers = getParticipatingPlayers(playerSelectBoxes);
				game.setScreen(new LevelSelectScreen(game, participatingPlayers));
				return;
			}
		}
	}

	public Array<PlayerInfo> getParticipatingPlayers(Array<PlayerSelectBox> players){
		Array<PlayerInfo> participatingPlayers = new Array<PlayerInfo>();
		for(PlayerSelectBox player : players){
			if(player.currentState != PlayerSelectBoxState.NOT_JOINED){
				participatingPlayers.add(new PlayerInfo(player.controller, player.playerNum, player.characterTypes[player.characterIndex]));
			}
		}
		return participatingPlayers;
	}

	@Override
	public void resize(int arg0, int arg1) {
	}

	@Override
	public void resume() {
	}

	@Override
	public void show() {
	}
	
	@Override
	public void dispose() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

}

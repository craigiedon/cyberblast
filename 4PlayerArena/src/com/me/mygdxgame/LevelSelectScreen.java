package com.me.mygdxgame;

import java.util.HashMap;

import com.artemis.Entity;
import com.artemis.World;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.me.mygdxgame.components.ControllerInfo;
import com.me.mygdxgame.components.PlayerInfo;
import com.me.mygdxgame.components.Position;
import com.me.mygdxgame.components.TextRenderable;
import com.me.mygdxgame.systems.TextRenderingSystem;

public class LevelSelectScreen implements Screen, ControllerListener {
	MyGdxGame game;
	Array<PlayerInfo> playerInfo;
	Array<LevelInfo> levels;
	int levelSelected;
	LevelSelectState optionSelected;
	boolean wasOptionChosen = false;
	int offsetX = 150;
	int offsetY = 130;
	World world;
	OrthographicCamera camera;
	HashMap<TileTag, TextureRegion> tileTagImage;
	TextureAtlas atlas;
	public final int previewTileSize = 30;
	public final float DEAD_ZONE = 0.75f;
	public final int HIGHLIGHT_HEIGHT = 50;
	Array<ControllerInfo> controllersInfo;
	
	public LevelSelectScreen(MyGdxGame g, Array<PlayerInfo> playerInfo){
		game = g;
		atlas = new TextureAtlas(Gdx.files.internal("placeHolderGraphics.atlas"));
		this.playerInfo = playerInfo;
		Json json = new Json();
		levels = new Array<LevelInfo>();
		levelSelected = 0;
		camera = new OrthographicCamera();
		camera.setToOrtho(false);
		optionSelected = LevelSelectState.PLAY;
		BitmapFont videoGameFont32 = game.videoGameFontGenerator.generateFont(32);
		videoGameFont32.setColor(Color.WHITE);
		
		
		FileHandle[] levelFiles = Gdx.files.local("customLevels/").list();
		for(FileHandle file : levelFiles){
			levels.add(json.fromJson(LevelInfo.class, file));
		}
		
		world = new World();
		world.setSystem(new TextRenderingSystem(camera));
		Entity titleText = world.createEntity();
		titleText.addComponent(new Position(camera.viewportWidth / 2, camera.viewportHeight - 40));
		titleText.addComponent(new TextRenderable("Choose Level", videoGameFont32 , true));
		titleText.addToWorld();
		
		Entity playText = world.createEntity();
		playText.addComponent(new Position(camera.viewportWidth / 2, 80));
		playText.addComponent(new TextRenderable("Play",  videoGameFont32, true));
		playText.addToWorld();
		
		Entity customLevelText = world.createEntity();
		customLevelText.addComponent((new Position(camera.viewportWidth / 2, 30)));
		customLevelText.addComponent((new TextRenderable("Custom Level", videoGameFont32, true)));
		customLevelText.addToWorld();
		
		tileTagImage = new HashMap<TileTag, TextureRegion>();
		tileTagImage.put(TileTag.FLOOR, atlas.findRegion("floorTile"));
		tileTagImage.put(TileTag.DESTRUCTIBLE_BLOCK, atlas.findRegion("destructibleTile"));
		tileTagImage.put(TileTag.HOLE, atlas.findRegion("holeTile"));
		tileTagImage.put(TileTag.INDESTRUCTIBLE_BLOCK, atlas.findRegion("indestructibleTile"));
		tileTagImage.put(TileTag.P1_START, atlas.findRegion("p1Start"));
		tileTagImage.put(TileTag.P2_START, atlas.findRegion("p2Start"));
		tileTagImage.put(TileTag.P3_START, atlas.findRegion("p3Start"));
		tileTagImage.put(TileTag.P4_START, atlas.findRegion("p4Start"));
		
		Controllers.addListener(this);
		controllersInfo = new Array<ControllerInfo>();
		for(Controller controller : Controllers.getControllers()){
			controllersInfo.add(new ControllerInfo(controller, ThumbStickPos.DEAD_ZONE));
		}
		world.initialize();
		
	}

	@Override
	public void render(float arg0) {
		Gdx.gl.glClearColor(0,0,0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		camera.update();
		game.batch.setProjectionMatrix(camera.combined);
		
		game.batch.begin();
		TileTag[][] currentLevelTiles = levels.get(levelSelected).levelTiles;
		for(int row = 0; row < currentLevelTiles.length; row++){
			for(int col = 0; col < currentLevelTiles[row].length; col++){
				game.batch.draw(tileTagImage.get(currentLevelTiles[row][col]), offsetX + col * previewTileSize, offsetY + row * previewTileSize, previewTileSize, previewTileSize);
			}
		}
		
		if(optionSelected == LevelSelectState.PLAY){
			game.batch.draw(atlas.findRegion("exampleBlock6"), 0, 80 - HIGHLIGHT_HEIGHT / 2, camera.viewportWidth, HIGHLIGHT_HEIGHT);
		}
		else{
			game.batch.draw(atlas.findRegion("exampleBlock6"), 0, 30 - HIGHLIGHT_HEIGHT / 2, camera.viewportWidth, HIGHLIGHT_HEIGHT);
		}
		
		game.batch.draw(atlas.findRegion("leftArrow"), 20, 300);
		game.batch.draw(atlas.findRegion("rightArrow"), camera.viewportWidth - 120, 300);
		game.batch.end();
		
		world.process();
		
		if(wasOptionChosen){
			Controllers.removeListener(this);
			if(optionSelected == LevelSelectState.PLAY){

				game.setScreen(new GameScreen(game, playerInfo, levels.get(levelSelected)));
			}
			else{
				game.setScreen(new CustomLevelScreen(game, playerInfo));
			}
		}

	}

	@Override
	public void resize(int arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean accelerometerMoved(Controller arg0, int arg1, Vector3 arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean axisMoved(Controller controller, int axisCode, float value) {
		if(axisCode == XBoxConfig.AXIS_LEFT_X || axisCode == XBoxConfig.AXIS_LEFT_Y){
			float x = controller.getAxis(XBoxConfig.AXIS_LEFT_X);
			float y = controller.getAxis(XBoxConfig.AXIS_LEFT_Y);
			
			ThumbStickPos direction = XBoxConfig.convertTo4WayDirection(x, y, DEAD_ZONE);
			for(ControllerInfo contInfo : controllersInfo){
				if(contInfo.controller.equals(controller)){
					if(direction != contInfo.prevLeftAnalogueState){
						switch(direction){
						case UP:
							if(optionSelected == LevelSelectState.PLAY){
								optionSelected = LevelSelectState.CUSTOM_LEVEL;
							}
							else{
								optionSelected = LevelSelectState.PLAY;
							}
							break;
						case DOWN:
							if(optionSelected == LevelSelectState.PLAY){
								optionSelected = LevelSelectState.CUSTOM_LEVEL;
							}
							else{
								optionSelected = LevelSelectState.PLAY;
							}
							break;
						case LEFT:
							levelSelected--;
							if(levelSelected < 0){
								levelSelected = levels.size - 1;
							}
							break;
						case RIGHT:
							levelSelected = (levelSelected + 1) % levels.size;
							break;
						default:
							break;
						}
						contInfo.prevLeftAnalogueState = direction;
					}
				}
			}
		}
		return false;
	}

	@Override
	public boolean buttonDown(Controller controller, int buttonCode) {
		if(buttonCode == XBoxConfig.BUTTON_A || buttonCode == XBoxConfig.BUTTON_START){
			wasOptionChosen = true;
		}
		return false;
	}

	@Override
	public boolean buttonUp(Controller arg0, int arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void connected(Controller arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disconnected(Controller arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean povMoved(Controller controller, int povCode, PovDirection povVal) {
		if(povVal == PovDirection.west){
			levelSelected--;
			if(levelSelected < 0){
				levelSelected = levels.size - 1;
			}
		}
		else if(povVal == PovDirection.east){
			levelSelected = (levelSelected + 1) % levels.size;
		}
		else if(povVal == PovDirection.north || povVal == PovDirection.south){
			if(optionSelected == LevelSelectState.PLAY){
				optionSelected = LevelSelectState.CUSTOM_LEVEL;
			}
			else{
				optionSelected = LevelSelectState.PLAY;
			}
		}
		return false;
	}

	@Override
	public boolean xSliderMoved(Controller arg0, int arg1, boolean arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean ySliderMoved(Controller arg0, int arg1, boolean arg2) {
		// TODO Auto-generated method stub
		return false;
	}

}

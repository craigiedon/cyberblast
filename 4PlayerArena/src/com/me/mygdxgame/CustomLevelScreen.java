package com.me.mygdxgame;

import com.artemis.Entity;
import com.artemis.World;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.me.mygdxgame.components.PlayerInfo;
import com.me.mygdxgame.components.Position;
import com.me.mygdxgame.components.TextRenderable;
import com.me.mygdxgame.systems.TextRenderingSystem;

public class CustomLevelScreen implements Screen {
	
	World world;
	final MyGdxGame game;
	Stage stage;
	Array<PlayerInfo> playerInfo;
	OrthographicCamera camera;
	ScrollPane levelScrollPane;
	List levelsList;
	
	public CustomLevelScreen(MyGdxGame g, Array<PlayerInfo> playerInf){
		this.game = g;
		this.playerInfo = playerInf;
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);
		camera = new OrthographicCamera();
		camera.setToOrtho(false);
		BitmapFont videoGameFont32 = game.videoGameFontGenerator.generateFont(32);
		videoGameFont32.setColor(Color.WHITE);
		
		if(!Gdx.files.local("customLevels").exists()){
			System.out.println("Folder does not exist, creating customLevels folder");
			Gdx.files.local("customLevels").mkdirs();
		}
		
		Array<String> levels = new Array<String>();
		for(FileHandle file : Gdx.files.local("customLevels").list()){
			levels.add(file.name());
		}
		
		levelsList = new List(levels.toArray(), game.skin);
		levelScrollPane = new ScrollPane(levelsList, game.skin);
		levelScrollPane.setPosition(camera.viewportWidth / 2 - levelScrollPane.getWidth() / 2, 200);
		
		
		TextButton loadLevelButton = new TextButton("Load", game.skin);
		loadLevelButton.setPosition(camera.viewportWidth / 2 - loadLevelButton.getWidth() / 2, 150);
		loadLevelButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y){
				String selectedLevelName = levelsList.getSelection();
				if(selectedLevelName != null){
					Json json = new Json();
					LevelInfo customLevel = json.fromJson(LevelInfo.class, Gdx.files.local("customLevels/" + selectedLevelName));
					game.setScreen(new GameScreen(game, playerInfo, customLevel));
				}
			}
		});
		TextButton backButton = new TextButton("Back", game.skin);
		backButton.setPosition(camera.viewportWidth / 2 - backButton.getWidth() / 2, 100);
		backButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y){
				game.setScreen(new LevelSelectScreen(game, playerInfo));
			}
		});
		
		stage.addActor(levelScrollPane);
		stage.addActor(loadLevelButton);
		stage.addActor(backButton);
		
		world = new World();
		world.setSystem(new TextRenderingSystem(camera));
		
		Entity titleText = world.createEntity();
		titleText.addComponent(new Position(camera.viewportWidth / 2, camera.viewportHeight - 125));
		titleText.addComponent(new TextRenderable("Custom Level", videoGameFont32, true));
		titleText.addToWorld();
		world.initialize();
		
	}
	
	@Override
	public void render(float arg0) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		camera.update();
		game.batch.setProjectionMatrix(camera.combined);
		
		
		
		stage.draw();
		stage.act();
		
		world.process();
	}

	@Override
	public void resize(int arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}


}

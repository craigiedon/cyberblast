package com.me.mygdxgame;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;


public class MyGdxGame extends Game{
	public SpriteBatch batch;
	public Skin skin;
	public ShapeRenderer shapeRenderer;
	public BitmapFont font;
	FreeTypeFontGenerator videoGameFontGenerator;
	
	@Override
	public void create() {
		batch = new SpriteBatch();
		shapeRenderer = new ShapeRenderer();
		font = new BitmapFont();
		skin = new Skin(Gdx.files.internal("uiskin.json"));
		videoGameFontGenerator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/8BitWonder.ttf"));
//		this.setScreen(new StartScreen(this));
		this.setScreen(new GameScreen(this));
//		this.setScreen((new PlayerSelectScreen(this)));
//		this.setScreen(new LevelEditorScreen(this));
	}
	
	@Override
	public void render(){
		super.render();
	}
	
	@Override
	public void dispose(){
		batch.dispose();
		font.dispose();
		shapeRenderer.dispose();
	}
}

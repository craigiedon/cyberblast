package com.me.mygdxgame;

import com.artemis.Entity;
import com.artemis.World;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector3;
import com.me.mygdxgame.components.Position;
import com.me.mygdxgame.components.Renderable;
import com.me.mygdxgame.components.TextRenderable;

public class PlayerSelectBox implements ControllerListener {

	World world;
	int playerNum;
	Controller controller;
	ThumbStickPos prevDirection = ThumbStickPos.DEAD_ZONE;
	PlayerSelectBoxState currentState;
	int xPos;
	int yPos;
	final float DEAD_ZONE = 0.75f;
	TextureAtlas atlas;
	CharacterType[] characterTypes;
	int characterIndex;
	Entity charSelectText;
	BitmapFont videoGameFont16;
	BitmapFont font;
	Entity joinText;
	Entity startText;
	Entity leftArrow;
	Entity rightArrow;
	
	public PlayerSelectBox(World world, int n, TextureAtlas atlas, Controller controller, BitmapFont font) {
		this.world = world;
		playerNum = n;
		this.atlas = atlas;
		this.controller = controller;
		this.font = font;
		characterTypes = CharacterType.values();
		characterIndex = (playerNum - 1) % characterTypes.length;
		currentState = PlayerSelectBoxState.NOT_JOINED;
		
		xPos = 200 + ((playerNum + 1) % 2) * 400;
		if(playerNum <= 2){
			yPos = 150 + 300;
		}
		else{
			yPos = 150;
		}
		
		joinText = world.createEntity();
		joinText.addComponent(new Position(xPos, yPos));
		joinText.addComponent(new TextRenderable("Press start to join", font, true));
		joinText.addToWorld();
		
		leftArrow = world.createEntity();
		leftArrow.addComponent(new Position(xPos - 100, yPos - 10));
		leftArrow.addComponent(new Renderable(atlas.findRegion("leftArrow"), 20,20));
		
		rightArrow = world.createEntity();
		rightArrow.addComponent(new Position(xPos + 80, yPos - 10));
		rightArrow.addComponent(new Renderable(atlas.findRegion("rightArrow"), 20, 20));
	}

	@Override
	public boolean buttonDown(Controller controller, int buttonCode) {
		if(currentState == PlayerSelectBoxState.NOT_JOINED){
			if(buttonCode == XBoxConfig.BUTTON_START || buttonCode == XBoxConfig.BUTTON_A){
				
				Entity player = world.createEntity();
				player.addComponent(new Renderable(atlas.findRegion("robotWalk" + playerNum), 35, 35));
				player.addComponent(new Position(xPos - 17, yPos - 17));
				player.addToWorld();
				
				charSelectText = world.createEntity();
				charSelectText.addComponent(new TextRenderable(characterTypes[characterIndex].toString(), font, true));
				charSelectText.addComponent(new Position(xPos, yPos - 30));
				charSelectText.addToWorld();
				
				joinText.deleteFromWorld();
				leftArrow.addToWorld();
				rightArrow.addToWorld();
				currentState = PlayerSelectBoxState.SELECT_PLAYER;
			}
		}
		else if(currentState == PlayerSelectBoxState.SELECT_PLAYER){
			if(buttonCode == XBoxConfig.BUTTON_RB){
				characterIndex = (characterIndex + 1) % characterTypes.length;
				updateDisplay();
			}
			else if(buttonCode == XBoxConfig.BUTTON_LB){
				characterIndex = characterIndex == 0 ? (characterTypes.length - 1) : (characterIndex - 1);
				updateDisplay();
			}
			else if(buttonCode == XBoxConfig.BUTTON_START || buttonCode == XBoxConfig.BUTTON_A){
				currentState = PlayerSelectBoxState.PLAYER_SELECTED;
				startText = world.createEntity();
				startText.addComponent(new Position(xPos, yPos - 55));
				startText.addComponent(new TextRenderable("Press start to begin match", font, true));
				startText.addToWorld();
				leftArrow.deleteFromWorld();
				rightArrow.deleteFromWorld();
			}
		}
		else if(currentState == PlayerSelectBoxState.PLAYER_SELECTED){
			if(buttonCode == XBoxConfig.BUTTON_START){
				currentState = PlayerSelectBoxState.START_REQUESTED;
				startText.deleteFromWorld();
			}
		}
		return false;
	}

	@Override
	public boolean buttonUp(Controller arg0, int arg1) {
		return false;
	}

	@Override
	public void connected(Controller arg0) {
	}

	@Override
	public void disconnected(Controller arg0) {
	}

	@Override
	public boolean povMoved(Controller cont, int povType, PovDirection direction) {
		if(currentState == PlayerSelectBoxState.SELECT_PLAYER){
			if(direction.equals(PovDirection.east)){
				characterIndex--;
				if(characterIndex < 0){
					characterIndex = characterTypes.length - 1;
				}
				updateDisplay();
			}
			else if(direction.equals(PovDirection.west)){
				characterIndex = (characterIndex + 1) % characterTypes.length;
				updateDisplay();
			}
		}
		return false;
	}

	@Override
	public boolean xSliderMoved(Controller arg0, int arg1, boolean arg2) {
		return false;
	}

	@Override
	public boolean ySliderMoved(Controller arg0, int arg1, boolean arg2) {
		return false;
	}
	
	@Override
	public boolean accelerometerMoved(Controller arg0, int arg1, Vector3 arg2) {
		return false;
	}

	@Override
	public boolean axisMoved(Controller cont, int axisCode, float value) {
		ThumbStickPos direction = XBoxConfig.convertToLeftRight(cont.getAxis(XBoxConfig.AXIS_LEFT_X), DEAD_ZONE);
		
		if(currentState == PlayerSelectBoxState.SELECT_PLAYER && direction != prevDirection){
			if(direction == ThumbStickPos.LEFT){
				characterIndex--;
				if(characterIndex < 0){
					characterIndex = characterTypes.length - 1; 
				}
				updateDisplay();
			}
			else if(direction == ThumbStickPos.RIGHT){
				characterIndex = (characterIndex + 1) % characterTypes.length;
				updateDisplay();
			}
			prevDirection = direction;
		}
		return false;
	}
	
	public void updateDisplay(){
		charSelectText.getComponent(TextRenderable.class).text = characterTypes[characterIndex].toString();
	}


}

package com.me.mygdxgame;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.me.mygdxgame.components.PlayerInfo;
import com.me.mygdxgame.systems.LevelSystem;

public class LevelEditorScreen implements Screen{
	TileTag[][] levelTiles;
	int nRows = 13;
	int nCols = 17;
	int miniTileSize = 35;
	int gridOffsetX = 10;
	int gridOffsetY = 20;
	MyGdxGame game;
	OrthographicCamera camera;
	HashMap<TileTag, TextureRegion> tileTagImage;
	TileTag currentTool;
	TextureAtlas atlas;
	Stage stage;
	
	ArrayList<String> levelNames;
	SelectBox levelSelectBox;
	
	public LevelEditorScreen(MyGdxGame g){
		levelTiles = new TileTag[nRows][nCols];
		game = g;
		camera = new OrthographicCamera();
		camera.setToOrtho(false);
		atlas = new TextureAtlas(Gdx.files.internal("placeHolderGraphics.atlas"));
		tileTagImage = new HashMap<TileTag, TextureRegion>();
		currentTool = TileTag.FLOOR;
		
		for(int i = 0; i < levelTiles.length; i++){
			for(int j = 0; j < levelTiles[i].length; j++){
				levelTiles[i][j] = TileTag.FLOOR;
			}
		}
		
		tileTagImage.put(TileTag.FLOOR, atlas.findRegion("floorTile"));
		tileTagImage.put(TileTag.DESTRUCTIBLE_BLOCK, atlas.findRegion("destructibleTile"));
		tileTagImage.put(TileTag.HOLE, atlas.findRegion("holeTile"));
		tileTagImage.put(TileTag.INDESTRUCTIBLE_BLOCK, atlas.findRegion("indestructibleTile"));
		tileTagImage.put(TileTag.P1_START, atlas.findRegion("p1Start"));
		tileTagImage.put(TileTag.P2_START, atlas.findRegion("p2Start"));
		tileTagImage.put(TileTag.P3_START, atlas.findRegion("p3Start"));
		tileTagImage.put(TileTag.P4_START, atlas.findRegion("p4Start"));
		
		stage = new Stage();
		VerticalGroup v1 = new VerticalGroup();
		HorizontalGroup h1 = new HorizontalGroup();
		HorizontalGroup h2 = new HorizontalGroup();
		HorizontalGroup h3 = new HorizontalGroup();
		HorizontalGroup h4 = new HorizontalGroup();
		
		v1.setSpacing(5);
		h1.setSpacing(5);
		h2.setSpacing(5);
		h3.setSpacing(5);
		h4.setSpacing(5);
		v1.setPosition(675, 500);
		
		Label toolsLabel = new Label("Tools", game.skin);
		v1.addActor(toolsLabel);
		
		TileTag[] tagValues = TileTag.values();
		for(int i = 0; i < tagValues.length / 2; i++){
			final TileTag tag = tagValues[i];
			TextureRegion texRegion = tileTagImage.get(tag);
			ImageButton imgButton = new ImageButton(new TextureRegionDrawable(texRegion));
			imgButton.addListener(new ClickListener(){
				@Override
				public void clicked(InputEvent event, float x, float y){
					currentTool = tag;
				}
			});
			
			if((i % 2) == 0){
				h1.addActor(imgButton);
			}
			else{
				h2.addActor(imgButton);
			}
		}
		
		for(int i = tagValues.length / 2; i < tagValues.length; i++){
			final TileTag tag = tagValues[i];
			TextureRegion texRegion = tileTagImage.get(tag);
			ImageButton imgButton = new ImageButton(new TextureRegionDrawable(texRegion));
			imgButton.addListener(new ClickListener(){
				@Override
				public void clicked(InputEvent event, float x, float y){
					currentTool = tag;
				}
			});
			
			if((i % 2) == 0){
				h3.addActor(imgButton);
			}
			else{
				h4.addActor(imgButton);
			}
		}
		
		v1.addActor(h1);
		v1.addActor(h2);
		v1.addActor(h3);
		v1.addActor(h4);
		
		FileHandle[] fileHandles = Gdx.files.local("customLevels/").list();
		String[] fileNames = new String[fileHandles.length];
		for(int i = 0; i < fileNames.length; i++){
			fileNames[i] = fileHandles[i].name();
		}
		
		levelNames = new ArrayList<String>(Arrays.asList(fileNames));
		levelSelectBox = new SelectBox(fileNames, game.skin);
		
		TextButton newButton = new TextButton("New", game.skin);
		newButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y){
				createNewLevel();
			}
		});
		
		TextButton saveButton = new TextButton("Save", game.skin);
		saveButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y){
				saveLevel(levelSelectBox.getSelection());
			}
		});
		TextButton loadButton = new TextButton("Load", game.skin);
		loadButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y){
				loadLevel(levelSelectBox.getSelection());
			}
		});
		TextButton clearButton = new TextButton("Clear", game.skin);
		clearButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y){
				clearLevel();
			}
		});
		TextButton testButton = new TextButton("Test", game.skin);
		testButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y){
				saveLevel(levelSelectBox.getSelection());
				LevelInfo levelInfo = new LevelInfo(levelTiles);
				Array<PlayerInfo> participatingPlayers = new Array<PlayerInfo>();
				Array<Controller> controllers = Controllers.getControllers();
				CharacterType[] charTypes = CharacterType.values();
				for(int i = 0; i < controllers.size; i++){
					Controller controller = controllers.get(i);
					participatingPlayers.add(new PlayerInfo(controller, i, charTypes[i]));
				}
				game.setScreen(new GameScreen(game, participatingPlayers, levelInfo));
			}
		});
		TextButton backButton = new TextButton("Back to Menu" , game.skin);
		backButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y){
				game.setScreen(new StartScreen(game));
			}
		});
		
		v1.addActor(levelSelectBox);
		v1.addActor(newButton);
		v1.addActor(saveButton);
		v1.addActor(loadButton);
		v1.addActor(clearButton);
		v1.addActor(testButton);
		v1.addActor(backButton);

		stage.addActor(v1);
		Gdx.input.setInputProcessor(stage);
		
		
		if(levelNames != null && levelNames.size() > 0){
			loadLevel(levelNames.get(0));
		}
		else{
			createNewLevel();
		}
		
	}
	
	@Override
	public void render(float arg0) {
		Gdx.gl.glClearColor(0.5f, 0.5f, 1f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		camera.update();
		game.batch.setProjectionMatrix(camera.combined);
		game.shapeRenderer.setProjectionMatrix(camera.combined);
		
		game.batch.begin();
		
		for(int row = 0; row < levelTiles.length; row++){
			for(int col = 0; col < levelTiles[row].length; col++){
				TileTag tileTag = levelTiles[row][col];
				game.batch.draw(tileTagImage.get(tileTag), gridOffsetX + col * miniTileSize, gridOffsetY + row * miniTileSize, miniTileSize, miniTileSize);
			}
		}
		
		Vector3 mousePos = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
		camera.unproject(mousePos);
		mousePos.x -= gridOffsetX;
		mousePos.y -= gridOffsetY;
		int mouseTileRow = (int) (mousePos.y / miniTileSize);
		int mouseTileCol = (int) (mousePos.x / miniTileSize);
		game.batch.setColor(1,1,1,0.5f);
		if(mouseTileRow < levelTiles.length &&
		   mouseTileRow >= 0 &&
		   mouseTileCol < levelTiles[mouseTileRow].length &&
		   mouseTileCol >= 0){
			game.batch.draw(atlas.findRegion("exampleBlock1"), gridOffsetX + mouseTileCol * miniTileSize, gridOffsetY + mouseTileRow * miniTileSize, miniTileSize, miniTileSize);
			if(Gdx.input.isTouched()){
				if(mouseTileRow != levelTiles.length - 1 && mouseTileRow != 0 && mouseTileCol != 0 && mouseTileCol != levelTiles[mouseTileRow].length - 1){
					levelTiles[mouseTileRow][mouseTileCol] = currentTool;
				}
			}
		}
		game.batch.setColor(1,1,1,1);
		
		game.batch.end();
		
		game.shapeRenderer.begin(ShapeType.Line);
		game.shapeRenderer.setColor(Color.BLACK);
		for(int row = 0; row < levelTiles.length; row++){
			for(int col = 0; col < levelTiles[row].length; col++){
				game.shapeRenderer.rect(gridOffsetX + col * miniTileSize, gridOffsetY + row * miniTileSize, miniTileSize, miniTileSize);
			}
		}
		game.shapeRenderer.end();
		
		stage.act();
		stage.draw();
	}
	
	public void loadLevel(String levelName){
		LevelInfo levelToLoad;
		Json json = new Json();
		FileHandle loadFile = Gdx.files.local("customLevels/" + levelName);				
		levelToLoad = json.fromJson(LevelInfo.class, loadFile);
		levelTiles = levelToLoad.levelTiles;
	}
	
	public void saveLevel(String levelName){
		LevelInfo levelToSave = new LevelInfo(levelTiles);
		Json json = new Json();
		String levelJsonData = json.toJson(levelToSave);
		FileHandle saveFile = Gdx.files.local("customLevels/" + levelName);
		saveFile.writeString(levelJsonData, false);
	}
	
	public void clearLevel(){
		for(int row = 0; row < levelTiles.length; row++){
			for(int col = 0; col < levelTiles[row].length; col++){
				if(row == levelTiles.length - 1 || row == 0 || col == levelTiles[row].length - 1 || col == 0){
					levelTiles[row][col] = TileTag.INDESTRUCTIBLE_BLOCK;
				}
				else{
					levelTiles[row][col] = TileTag.FLOOR;
				}
			}
		}
	}
	
	public  void createNewLevel(){
		Pattern levelNameRegex = Pattern.compile("level(\\d*)\\.json");
		//Get a dir list of all levels
		FileHandle[] directoryListing = Gdx.files.local("customLevels/").list();
		//For every level
		int maxLevelNum = 0;
		for(FileHandle file : directoryListing){
			//Get its name, cut off level part from the start and then json part from the end with a regular expression
			Matcher levelNameMatcher = levelNameRegex.matcher(file.name());
			if(levelNameMatcher.matches()){
				int currentLevelNum = Integer.parseInt(levelNameMatcher.group(1));
				//Store the max number
				if(currentLevelNum > maxLevelNum){
					maxLevelNum = currentLevelNum;
				}
			}
		}

		String newLevelName = "level" + (maxLevelNum + 1) + ".json";
		clearLevel();
		saveLevel(newLevelName);
		levelNames.add(newLevelName);
		levelSelectBox.setItems(levelNames.toArray());
		levelSelectBox.setSelection(levelNames.size() - 1);
	}

	@Override
	public void resize(int arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume(){

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}
}

package com.me.mygdxgame;

public enum BombBehaviour {
	NORMAL, REMOTE, HOMING, NAPALM
}

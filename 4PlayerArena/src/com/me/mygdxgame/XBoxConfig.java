package com.me.mygdxgame;

import com.badlogic.gdx.controllers.PovDirection;

public class XBoxConfig {
	public static final int BUTTON_X = 2;
	public static final int BUTTON_Y = 3;
	public static final int BUTTON_A = 0;
	public static final int BUTTON_B = 1;
	public static final int BUTTON_START = 7;
	public static final PovDirection BUTTON_DPAD_UP = PovDirection.north;
	public static final PovDirection BUTTON_DPAD_DOWN = PovDirection.south;
	public static final PovDirection BUTTON_DPAD_RIGHT = PovDirection.east;
	public static final PovDirection BUTTON_DPAD_LEFT = PovDirection.west;
	public static final int BUTTON_LB = 4;
	public static final int BUTTON_L3 = 8;
	public static final int BUTTON_RB = 5;
	public static final int BUTTON_R3 = 9;
	public static final int AXIS_LEFT_X = 1;
	public static final int AXIS_LEFT_Y = 0;
	public static final int AXIS_LEFT_TRIGGER = 4;
	public static final int AXIS_RIGHT_X = 3;
	public static final int AXIS_RIGHT_Y = 2;
	public static final int AXIS_RIGHT_TRIGGER = 4;
	
	public static ThumbStickPos convertToUpDown(float x, float y, float deadZone){
		if(Math.abs(y) <= deadZone){
			return ThumbStickPos.DEAD_ZONE;
		}
		else if(y > 0){
			return ThumbStickPos.UP;
		}
		else{
			return ThumbStickPos.DOWN;
		}
	}
	
	public static ThumbStickPos convertTo4WayDirection(float x, float y, float deadZone){
		if(Math.abs(y) <= deadZone && Math.abs(x) <= deadZone){
			return ThumbStickPos.DEAD_ZONE;
		}
		
		if(Math.abs(y) > Math.abs(x)){
			if(y > 0){
				return ThumbStickPos.UP;
			}
			else{
				return ThumbStickPos.DOWN;
			}
		}
		else{
			if(x > 0){
				return ThumbStickPos.LEFT;
			}
			else{
				return ThumbStickPos.RIGHT;
			}
		}
	}
	
	public static ThumbStickPos convertToLeftRight(float x, float deadZone){
		if(Math.abs(x) <= deadZone){
			return ThumbStickPos.DEAD_ZONE;
		}
		else if(x > 0){
			return ThumbStickPos.RIGHT;
		}
		else{
			return ThumbStickPos.LEFT;
		}
	}
}

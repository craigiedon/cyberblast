package com.me.mygdxgame.components;

public enum PowerType {
	SPEED_BOOST, BOMB_SPREAD, BOMB_AMOUNT;
}

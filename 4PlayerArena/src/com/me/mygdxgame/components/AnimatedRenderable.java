package com.me.mygdxgame.components;

import java.util.HashMap;

import com.artemis.Component;
import com.badlogic.gdx.graphics.g2d.Animation;

public class AnimatedRenderable extends Component{
	private HashMap<AnimationTag, Animation> animations;
	private AnimationTag currentAnimation;
	public float currentTime;
	
	public AnimatedRenderable(AnimationTag tag, Animation animation){
		animations = new HashMap<AnimationTag, Animation>();
		currentAnimation = tag;
		animations.put(tag, animation);
		currentTime = 0;
	}
	
	public Animation getCurrentAnimation(){
		return animations.get(currentAnimation);
	}

}

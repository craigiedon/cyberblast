package com.me.mygdxgame.components;

import com.artemis.Component;

public class Moveable extends Component {
	public float vx;
	public float vy;
	public float ax;
	public float ay;
	public float maxVX;
	public float maxVY;
	
	public Moveable(float x, float y, float maxX, float maxY){
		vx = x;
		vy = y;
		ax = 0;
		ay = 0;
		maxVX = maxX;
		maxVY = maxY;
	}
	
	public Moveable(float x, float y){
		this(x,y,0,0);
	}
	
	public Moveable(){
		this(0,0,0,0);
	}
}

package com.me.mygdxgame.components;

import com.artemis.Component;
import com.badlogic.gdx.controllers.Controller;
import com.me.mygdxgame.CharacterType;

public class PlayerInfo extends Component {
	public Controller controller;
	public int playerNum;
	public CharacterType charType;
	
	public PlayerInfo(Controller cont, int playerNum, CharacterType c){
		controller = cont;
		this.playerNum = playerNum;
		charType = c;
	}
}

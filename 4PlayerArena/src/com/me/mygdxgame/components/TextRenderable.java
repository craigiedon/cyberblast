package com.me.mygdxgame.components;

import com.artemis.Component;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;

public class TextRenderable extends Component{
	public String text;
	public BitmapFont font;
	public boolean centred;
	
	
	public TextRenderable(String txt, BitmapFont font, boolean centred){
		text = txt;
		this.font = font;
		this.centred = centred;
	}
	
	public TextRenderable(String txt, BitmapFont font){
		this(txt, font, false);
	}
	
	public TextBounds getDimensions(){
		return font.getBounds(text);
	}

}

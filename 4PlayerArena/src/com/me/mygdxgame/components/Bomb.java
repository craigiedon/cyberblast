package com.me.mygdxgame.components;

import com.artemis.Component;
import com.me.mygdxgame.BombBehaviour;

public class Bomb extends Component {
	public float expiryTime;
	public BombBehaviour bombBehaviour;
	public int spread;
	
	public Bomb(float expTime, BombBehaviour beh, int spread){
		expiryTime = expTime;
		bombBehaviour = beh;
		this.spread = spread;
	}
}

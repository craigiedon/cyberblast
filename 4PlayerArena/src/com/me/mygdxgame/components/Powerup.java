package com.me.mygdxgame.components;

import com.artemis.Component;

public class Powerup extends Component {
	public PowerType powerType;
	
	public Powerup(PowerType pt){
		powerType = pt;
	}
}

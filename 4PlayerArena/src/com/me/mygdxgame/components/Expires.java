package com.me.mygdxgame.components;

import com.artemis.Component;

public class Expires extends Component {
	public float timeLeft;
	
	public Expires(float expiryTime){
		timeLeft = expiryTime;
	}
}

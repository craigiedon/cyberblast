package com.me.mygdxgame.components;

import com.artemis.Component;

public class Inventory extends Component {
	public int bombsLeft;
	public int spreadingPower;
	public float moveSpeed;
	
	public Inventory(){
		this(1,1, 100);
	}
	
	public Inventory(int startingBombs, int startingSpread, float startSpeed){
		bombsLeft = startingBombs;
		spreadingPower = startingSpread;
		moveSpeed = startSpeed;
		
		
	}
}

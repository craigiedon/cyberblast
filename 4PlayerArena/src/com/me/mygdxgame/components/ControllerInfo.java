package com.me.mygdxgame.components;

import com.badlogic.gdx.controllers.Controller;
import com.me.mygdxgame.ThumbStickPos;


public class ControllerInfo{
	public Controller controller;
	public ThumbStickPos prevLeftAnalogueState;
	
	public ControllerInfo(Controller cont, ThumbStickPos originalLeftAnalogueState){
		prevLeftAnalogueState = originalLeftAnalogueState;
		controller = cont;
	}
}

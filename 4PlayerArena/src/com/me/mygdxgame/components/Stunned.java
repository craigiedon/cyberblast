package com.me.mygdxgame.components;

import com.artemis.Component;

public class Stunned extends Component{
	
	public float duration;
	
	public Stunned(float duration){
		this.duration = duration;
	}
	
}

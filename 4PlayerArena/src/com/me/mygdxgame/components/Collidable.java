package com.me.mygdxgame.components;

import com.artemis.Component;
import com.badlogic.gdx.math.Rectangle;

public class Collidable extends Component {
	public float width;
	public float height;
	
	public Collidable(float width, float height){
		this.width = width;
		this.height = height;
	}
	
	public Collidable(){
		width = 0;
		height = 0;
	}
}

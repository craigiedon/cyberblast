package com.me.mygdxgame.components;

import com.artemis.Component;

public class Position extends Component {
	public float x;
	public float y;
	
	public Position(float x, float y){
		this.x = x;
		this.y = y;
	}

	public Position() {
		x = 0;
		y = 0;
	}
}

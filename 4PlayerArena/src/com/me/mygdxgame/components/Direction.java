package com.me.mygdxgame.components;

public enum Direction {
	UP, DOWN, LEFT, RIGHT
}

package com.me.mygdxgame.components;

import com.artemis.Component;

public class Owned extends Component {
	public int playerNum;
	
	public Owned(int n){
		playerNum = n;
	}
}

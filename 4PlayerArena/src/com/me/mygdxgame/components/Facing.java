package com.me.mygdxgame.components;

import com.artemis.Component;

public class Facing extends Component {
	public Direction direction;
	
	public Facing(Direction dir){
		direction = dir;
	}
}

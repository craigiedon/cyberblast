package com.me.mygdxgame.components;

import com.artemis.Component;

public class Oscillates extends Component {
	public float oscillationStrengthX;
	public float oscillationStrengthY;
	
	public Oscillates(float osX, float osY){
		oscillationStrengthX = osX;
		oscillationStrengthY = osY;
	}
	
}

package com.me.mygdxgame.components;

import com.artemis.Component;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Renderable extends Component {
	private TextureRegion image;
	public int width;
	public int height;
	
	public Renderable(TextureRegion img){
		image = img;
		width = img.getRegionWidth();
		height = img.getRegionHeight();
	}
	
	public Renderable(TextureRegion img, int width, int height){
		image = img;
		this.width = width;
		this.height = height;
	}
	
	public TextureRegion getImage(){
		return image;
	}
	
	public void setImage(TextureRegion img){
		image = img;
	}
	
	public void setDimensions(int width, int height){
		this.width = width;
		this.height = height;
	}
	
	
}

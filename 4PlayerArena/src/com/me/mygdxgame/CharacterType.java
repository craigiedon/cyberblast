package com.me.mygdxgame;

public enum CharacterType {
	NORMAL, REMOTE, FORCE, NAPALM, HOMING
}

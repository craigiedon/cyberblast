package com.me.mygdxgame;

import com.artemis.Entity;
import com.artemis.World;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.me.mygdxgame.components.ControllerInfo;
import com.me.mygdxgame.components.PlayerInfo;
import com.me.mygdxgame.components.Position;
import com.me.mygdxgame.components.TextRenderable;
import com.me.mygdxgame.systems.TextRenderingSystem;

public class WinScreen implements Screen, ControllerListener{
	OrthographicCamera camera;
	World world;
	TextureAtlas atlas;
	MyGdxGame game;
	LevelInfo levelToReplay;
	Array<PlayerInfo> players;
	Array<ControllerInfo> controllersInfo;
	PlayerInfo winner;
	WinScreenState optionSelected;
	boolean selectPressed = false;
	final float DEAD_ZONE = 0.75f;
	
	public WinScreen(MyGdxGame g, LevelInfo originalLevelInfo, Array<PlayerInfo> participatingPlayers, PlayerInfo winningPlayer){
		game = g;
		camera = new OrthographicCamera();
		camera.setToOrtho(false);
		levelToReplay = originalLevelInfo;
		players = participatingPlayers;
		winner = winningPlayer;
		atlas = new TextureAtlas(Gdx.files.internal("placeHolderGraphics.atlas"));
		optionSelected = WinScreenState.REPLAY;
		
		BitmapFont videoGameFont32 = game.videoGameFontGenerator.generateFont(32);
		videoGameFont32.setColor(Color.WHITE);
		BitmapFont videoGameFont16 = game.videoGameFontGenerator.generateFont(16);
		videoGameFont16.setColor(Color.WHITE);
		
		world = new World();
		world.setSystem(new TextRenderingSystem(camera));
		
		Entity winText = world.createEntity();
		winText.addComponent(new Position(camera.viewportWidth / 2, camera.viewportHeight / 2 + 100));
		if(winningPlayer != null){
			winText.addComponent(new TextRenderable("Player " + winningPlayer.playerNum + " Wins" , videoGameFont32, true));
		}
		else{
			winText.addComponent(new TextRenderable("Its a Draw", videoGameFont32, true));
		}
		winText.addToWorld();
		
		Entity replayText = world.createEntity();
		replayText.addComponent(new Position(camera.viewportWidth / 2, 300));
		replayText.addComponent(new TextRenderable("Play Again", videoGameFont16, true));
		replayText.addToWorld();
		
		Entity mainMenuText = world.createEntity();
		mainMenuText.addComponent(new Position(camera.viewportWidth / 2, 200));
		mainMenuText.addComponent(new TextRenderable("Back to Main Menu", videoGameFont16, true));
		mainMenuText.addToWorld();
		
		controllersInfo = new Array<ControllerInfo>();
		for(Controller controller : Controllers.getControllers()){
			controllersInfo.add(new ControllerInfo(controller, ThumbStickPos.DEAD_ZONE));
		}
		
		Controllers.addListener(this);
		
		world.initialize();
		
	}

	@Override
	public void render(float arg0) {
		Gdx.gl.glClearColor(0,0,0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		camera.update();
		game.batch.setProjectionMatrix(camera.combined);
		
		game.batch.begin();
		if(optionSelected == WinScreenState.REPLAY){
			game.batch.draw(atlas.findRegion("exampleBlock6"), 0, 300 - 25, camera.viewportWidth, 50);

		}
		else if(optionSelected == WinScreenState.MAIN_MENU){
			game.batch.draw(atlas.findRegion("exampleBlock6"), 0, 200 - 25, camera.viewportWidth, 50);
		}
		game.batch.end();

		world.setDelta(Gdx.graphics.getDeltaTime());
		world.process();
		
		if(selectPressed){
			Controllers.removeListener(this);
			if(optionSelected == WinScreenState.REPLAY){
				game.setScreen(new GameScreen(game, players, levelToReplay));
			}
			else if(optionSelected == WinScreenState.MAIN_MENU){
				game.setScreen (new StartScreen(game));
			}
		}
	}

	@Override
	public void resize(int arg0, int arg1) {
	}

	@Override
	public void resume() {
	}

	@Override
	public void show() {
	}
	
	@Override
	public void dispose() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}



	@Override
	public boolean axisMoved(Controller cont, int axisCode, float value) {
		for(ControllerInfo controllerInfo : controllersInfo){
			if((axisCode == XBoxConfig.AXIS_LEFT_Y) && controllerInfo.controller.equals(cont)){
				ThumbStickPos direction = XBoxConfig.convertToUpDown(cont.getAxis(XBoxConfig.AXIS_LEFT_X), cont.getAxis(XBoxConfig.AXIS_LEFT_Y), DEAD_ZONE);
				if(direction != controllerInfo.prevLeftAnalogueState){
					if(direction == ThumbStickPos.UP || direction == ThumbStickPos.DOWN){
						switch(optionSelected){
						case MAIN_MENU:
							optionSelected = WinScreenState.REPLAY;
							break;
						case REPLAY:
							optionSelected = WinScreenState.MAIN_MENU;
							break;
						}
					}
					
					controllerInfo.prevLeftAnalogueState = direction;
				}
			}
		}
		return false;
	}

	@Override
	public boolean buttonDown(Controller controller, int buttonCode) {
		if(buttonCode == XBoxConfig.BUTTON_A || buttonCode == XBoxConfig.BUTTON_START){
			selectPressed = true;
		}
		return false;
	}

	@Override
	public boolean buttonUp(Controller arg0, int arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void connected(Controller arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disconnected(Controller arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean povMoved(Controller cont, int povCode, PovDirection direction) {
		if(direction == PovDirection.north || direction == PovDirection.south){
			switch(optionSelected){
			case REPLAY:
				optionSelected = WinScreenState.MAIN_MENU;
				break;
			case MAIN_MENU:
				optionSelected = WinScreenState.REPLAY;
			}
		}
		return false;
	}

	@Override
	public boolean xSliderMoved(Controller arg0, int arg1, boolean arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean ySliderMoved(Controller arg0, int arg1, boolean arg2) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean accelerometerMoved(Controller arg0, int arg1, Vector3 arg2) {
		// TODO Auto-generated method stub
		return false;
	}

}

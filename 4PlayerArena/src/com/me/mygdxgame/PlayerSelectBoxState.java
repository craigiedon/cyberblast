package com.me.mygdxgame;

public enum PlayerSelectBoxState {
	NOT_JOINED, SELECT_PLAYER, PLAYER_SELECTED, START_REQUESTED;
}

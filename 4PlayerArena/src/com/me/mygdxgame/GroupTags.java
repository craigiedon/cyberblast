package com.me.mygdxgame;

public class GroupTags {
	public static final String BLOCK = "Block";
	public static final String EXPLOSION = "Explosion";
	public static final String PLAYER = "Player";
	public static final String HOLE = "Hole";
	public static final String BOMB = "Bomb";
	public static final String POWERUP = "Powerup";
}
